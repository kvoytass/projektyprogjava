import java.util.ArrayList;

public class StockDataHolder extends AbstractBeansBindingModelObject
{

	private ArrayList<DailyStockInfoSeries> entries;
	
	public ArrayList<DailyStockInfoSeries> getEntries() {
		return entries;
	}
	
	public StockDataHolder() 
	{
		super();
		entries = new ArrayList<DailyStockInfoSeries>();
		LoadFromDatabase();
	}
	
	public void manualAdd(DailyStockInfoSeries dsis, SingleDayStockInfo toAdd)
	{
		ArrayList<DailyStockInfoSeries> oldV = entries;
		ArrayList<DailyStockInfoSeries> newV = (ArrayList<DailyStockInfoSeries>) entries.clone();
		
		dsis.add(toAdd);
		

        Thread t = new Thread(new Runnable()
        {
            public void run()
            {
            	System.out.println("Adding 1 entry to database ...");
    			LocalDBManager.AddSDSIToDatabase(toAdd);
            	System.out.println("1 entry added to database.");
            }
        });
        t.start();

		entries = newV;
		firePropertyChange("entries", oldV, newV);
	}

	public void add(DailyStockInfoSeries toAdd)
	{
		ArrayList<DailyStockInfoSeries> oldV = entries;
		ArrayList<DailyStockInfoSeries> newV = (ArrayList<DailyStockInfoSeries>) entries.clone();
		
		if(!Contains(toAdd.getSymbol()))
		{
			newV.add(toAdd);

            Thread t = new Thread(new Runnable()
            {
                public void run()
                {
                	System.out.println("Adding " + toAdd.getDailyInfos().size() + " entries to database ...");

                    ArrayList<SingleDayStockInfo> copy = (ArrayList<SingleDayStockInfo>) toAdd.getDailyInfos().clone();
                	
        			for(SingleDayStockInfo sdsi : copy)
        				LocalDBManager.AddSDSIToDatabase(sdsi);

                	System.out.println(toAdd.getDailyInfos().size() + " entries added to database.");
                }
            });
            t.start();
			
			System.out.println(toAdd.getSymbol() + " data added to dataholder and database!");
		}
		else
		{
			DailyStockInfoSeries alreadyIn = getDSIS(toAdd.getSymbol());
			ArrayList<SingleDayStockInfo> newEntries = alreadyIn.getNew(toAdd.getDailyInfos());
			alreadyIn.add(newEntries);

            Thread t = new Thread(new Runnable()
            {
                public void run()
                {
                	System.out.println("Adding " + newEntries.size() + " entries to database ...");
                	
        			for(SingleDayStockInfo sdsi : newEntries)
        				LocalDBManager.AddSDSIToDatabase(sdsi);

                	System.out.println(newEntries.size() + " entries added to database.");
                }
            });
            t.start();
			
			System.out.println(toAdd.getSymbol() + " data updated in dataholder and database!");
		}
		
		entries = newV;
		firePropertyChange("entries", oldV, newV);
	}
	
	void LoadFromDatabase()
	{
		ArrayList<DailyStockInfoSeries> oldV = entries;
		ArrayList<DailyStockInfoSeries> newV = (ArrayList<DailyStockInfoSeries>) entries.clone();
		
		if(!LocalDBManager.isInitialized()) LocalDBManager.initialize();
		newV = LocalDBManager.LoadDSISs();

		entries = newV;
		firePropertyChange("entries", oldV, newV);
	}
	
//	public void add(String symbol, ArrayList<SingleDayStockInfo> toAdd)
//	{
//		ArrayList<DailyStockInfoSeries> oldV = entries;
//		ArrayList<DailyStockInfoSeries> newV = (ArrayList<DailyStockInfoSeries>) entries.clone();
//		
//		DailyStockInfoSeries dsis = getDSIS(symbol);
//		if(dsis != null)
//		{
//			ArrayList<SingleDayStockInfo> notInEntries = dsis.getNew(toAdd);
//			for(SingleDayStockInfo ta : notInEntries) newV.add(ta);
//		}
//		else
//		{
//			newV.add(new DailyStockInfoSeries(symbol, toAdd));
//		}
//		
//		entries = newV;
//		firePropertyChange("entries", oldV, newV);
//	}
	
	public boolean Contains(String symbol)
	{
		for(DailyStockInfoSeries dsis : entries)
			if(dsis.getSymbol().equals(symbol)) return true;
		
		return false;
	}
	
	public DailyStockInfoSeries getDSIS(String symbol)
	{
		for(DailyStockInfoSeries dsis : entries)
			if(dsis.getSymbol().equals(symbol)) return dsis;
		
		return null;
	}
}
