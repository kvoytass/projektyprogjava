import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class InitFrame extends JFrame 
{

	private JPanel contentPane;
	JLabel lblStatus;
	private static boolean initResult = false;
	
	static InitFrame frame;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame = new InitFrame();
					frame.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	private void Init()
	{
		lblStatus.setText("Initalizing, please wait ...");
		initResult = RemoteDBManager.initialize(true);
		if(initResult)
		{
			lblStatus.setText("Initalizion finished :)");
		}
		else lblStatus.setText("Initalizion failed :(");
		

	    
		if(initResult)
		{
			frame.setVisible(false);
			LoginFrame lf = new LoginFrame();
			lf.setVisible(true);
		}
	}
	
	public InitFrame() 
	{
		setTitle("Initializing...");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 300, 160);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		lblStatus = new JLabel("Status");
		lblStatus.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lblStatus, BorderLayout.CENTER);
		
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);

		Thread t = new Thread(new Runnable() 
		{
		    public void run() 
		    {
				Init();
		    }
		});
		t.start();
	}

}
