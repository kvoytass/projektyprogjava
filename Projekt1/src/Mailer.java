import java.util.Properties;
import java.util.Random;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class Mailer 
{
	public static String generateActivationCode() { return generateActivationCode(6); }
	
	public static String generateActivationCode(int length)
	{
		String code = "";
		Random rand = new Random();
		for(int i = 0; i < length; i++) code += rand.nextInt(10);
		
		return code;
	}
	
	public static boolean SendEmail(DBConfig config, String toWhom, String title, String messageText)
	{
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", config.getEmail_host());
		props.put("mail.smtp.port", config.getEmail_port());

		Session session = Session.getInstance(props, new javax.mail.Authenticator() 
		{
			protected PasswordAuthentication getPasswordAuthentication() { return new PasswordAuthentication(config.getEmail_username(), config.getEmail_userpass()); } 
		});

		try 
		{
			System.out.println("Sending email message ...");
			Message message = new MimeMessage(session);
			
			message.setFrom(new InternetAddress(config.getEmail_senderAddress(), config.getEmail_senderDisplayName()));
			
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toWhom));
			
			message.setSubject(title);
			
			message.setText(messageText);
			
			Transport.send(message);
			
			System.out.println("Sent message successfully...");
			return true;
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			return false;
		}
	}
}
