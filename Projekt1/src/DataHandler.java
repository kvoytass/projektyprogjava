import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;

import org.json.JSONObject;

public class DataHandler 
{ 
	private static String readAll(Reader rd) 
	{
		try
		{
			StringBuilder sb = new StringBuilder();
			int cp;
			while ((cp = rd.read()) != -1) 
				sb.append((char) cp);
			
			return sb.toString();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return null;
	}
	
	private static String readAll(String url) 
	{
		try
		{
			InputStream is = new URL(url).openStream();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
			StringBuilder sb = new StringBuilder();
			int cp;
			while ((cp = rd.read()) != -1) 
				sb.append((char) cp);
			
			is.close();
			
			return sb.toString();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return null;
	}


	public static JSONObject readJsonFromUrl(String url)
	{
		try 
		{
			String jsonText = readAll(url);
			if(jsonText == null) return null; 
			JSONObject json = new JSONObject(jsonText);
			return json;
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return null;
	}
	
}
