import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.ArrayList;

import javax.swing.JFrame;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.SeriesException;
import org.jfree.data.time.Day;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class TimeChart extends ApplicationFrame 
{
	private static TimeChart instance;
	public static TimeChart getInstance(String title, DailyStockInfoSeries dsdi)
	{
		if(instance != null)
		{
			instance.dispose();
		}
		
		instance = new TimeChart(title, dsdi);
		instance.pack();
		
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		instance.setLocation(dim.width/2-instance.getSize().width/2, dim.height/2-instance.getSize().height/2);
		
		return instance;
	}
	
	private TimeChart(String title, DailyStockInfoSeries dsdi)
	{
		super(title);
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setUndecorated(true);

	    final XYDataset dataset = createDataset(dsdi);         
	    final JFreeChart chart = createChart(dataset, dsdi.getSymbol()); 
	    //chart.setDefaultCloseOperation(ApplicationFrame.DISPOSE_ON_CLOSE);
	    final ChartPanel chartPanel = new ChartPanel(chart);         
	    addKeyListener(new KeyAdapter() {
	    	@Override
	    	public void keyReleased(KeyEvent e) 
	    	{
	    		if(e.getKeyCode() == KeyEvent.VK_ESCAPE || e.getKeyCode() == KeyEvent.VK_SPACE)
	    		{
	    			dispose();
	    			instance = null;
	    		}
	    	}
	    });
	    chartPanel.setPreferredSize(new java.awt.Dimension(800 , 450));         
	    chartPanel.setMouseZoomable(true, false );         
	    setContentPane(chartPanel);
	}

	private XYDataset createDataset(DailyStockInfoSeries dsdi) 
	{
		ArrayList<SingleDayStockInfo> sdsiList = dsdi.getDailyInfos();
		sdsiList.sort(new SDSIComparator());
		
		final TimeSeries series = new TimeSeries("Data");         
			      
		for (SingleDayStockInfo sdsi : sdsiList) 
		{
			try 
			{            
				Day current = new Day(sdsi.getDateObject());
				series.add(current, new Double(sdsi.getClose()));
			}
			catch ( SeriesException e ) 
			{
				System.err.println("Error adding to series");
			}
		}
		
		return new TimeSeriesCollection(series);
	}

	private JFreeChart createChart(final XYDataset dataset, String symbol) 
	{
		return ChartFactory.createTimeSeriesChart(             
				symbol + " time series", 
				"Date",              
				"Close Price",              
				dataset,             
				false,              
				false,              
				false);
	}

	public static void main( final String[ ] args ) 
	{
	      final String title = "MyTitle";         
	      DailyStockInfoSeries dsis = DailyStockInfoSeries.parseFromWeb("AMZN");
	      final TimeChart demo = new TimeChart(title, dsis);         
	      demo.pack( );                 
	      demo.setVisible( true );
			
			Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
			demo.setLocation(dim.width/2-demo.getSize().width/2, dim.height/2-demo.getSize().height/2);
	}
}
