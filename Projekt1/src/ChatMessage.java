import java.lang.reflect.Field;

public class ChatMessage  implements DBEntry
{
	int message_id;
	String senderName;
	String dateSent;
	String text;
	
	public int getMessage_id() {
		return message_id;
	}

	public void setMessage_id(int id) {
		this.message_id = id;
	}

	public String getSenderName() {
		return senderName;
	}

	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	public String getDateSent() {
		return dateSent;
	}

	public void setDateSent(String dateSent) {
		this.dateSent = dateSent;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public ChatMessage(int message_id, String senderName, String dateSent, String text) {
		super();
		this.message_id = message_id;
		this.senderName = senderName;
		this.dateSent = dateSent;
		this.text = text;
	}
	

	@Override
	public String toDatabaseEntryFormat()
	{
		String tr = "";
		tr += message_id + ", ";
		tr += "'" + senderName + "', ";
		tr += "'" + dateSent + "', ";
		tr += "'" + text + "'";
				
		return tr;
	}

    public static String getDBTableColumnHeaders()
    {
        String tr = "";
        tr += "message_id int, ";
        tr += "senderName varchar (64), ";
        tr += "dateSent varchar (64), ";
        tr += "text varchar (1024)";

        return tr;
    }

	@Override
	public String toString() 
	{
		return "Message (" + message_id + ") sent on " + dateSent + " by user " + senderName + ":\n" + text;
	}


}
