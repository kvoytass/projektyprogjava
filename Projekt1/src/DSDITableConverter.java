import java.util.ArrayList;
import java.util.Collections;

import javax.swing.table.DefaultTableModel;

public class DSDITableConverter extends org.jdesktop.beansbinding.Converter<Object, DefaultTableModel>
{

	@Override
	public DefaultTableModel convertForward(Object arg0) 
	{
		DailyStockInfoSeries dsdi = (DailyStockInfoSeries)arg0;
		
		String[] columnNames = {"Date", "Open", "High", "Low", "Close", "Adjusted close", "Volume", "Dividend", "Split coef"};
		DefaultTableModel dtm = new DefaultTableModel(0,0);
		dtm.setColumnIdentifiers(columnNames);
		
		if(dsdi != null)
		{
			ArrayList<SingleDayStockInfo> list = dsdi.getDailyInfos();
			list.sort(new SDSIComparator());
			Collections.reverse(list);
			
			for(SingleDayStockInfo sdsi : list)
				dtm.addRow(new Object[] {sdsi.getDate(), sdsi.getOpen(), sdsi.getHigh(), sdsi.getLow(), sdsi.getClose(), sdsi.getAdjusted_close(), sdsi.getVolume(), sdsi.getDividend_amount(), sdsi.getSplit_coef()});
		}
		
		return dtm;
	}

	@Override
	public Object convertReverse(DefaultTableModel arg0) 
	{
		// TODO Auto-generated method stub
		return null;
	}

}
