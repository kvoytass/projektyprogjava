import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class MyTable extends JTable
{
	private DefaultTableModel dtm;

	public DefaultTableModel getDtm() 
	{
		return dtm;
	}

	public void setDtm(DefaultTableModel dtm) 
	{
		this.dtm = dtm;
		if(dtm != null) this.setModel(dtm);
	}
}
