import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import java.awt.GridBagLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.GridBagConstraints;
import javax.swing.JTextField;
import java.awt.Insets;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JSeparator;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.JTextPane;
import javax.swing.WindowConstants;
import javax.swing.JTextArea;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.SimpleDateFormat;

import org.jdesktop.beansbinding.BeanProperty;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Timer;
import java.util.TimerTask;

import org.jdesktop.beansbinding.AutoBinding;
import org.jdesktop.beansbinding.Bindings;
import org.jfree.ui.ApplicationFrame;
import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import javax.swing.JScrollPane;

public class MainFrame extends JFrame 
{

	private JPanel contentPane;
	private JTextField textField_symbol;
	private MyTable table_stockInfo;
	private JTextField textField_chatMessage;
	private JTextArea textArea_chat;
	private MyComboBox comboBox_entries;
	JButton btnChatRefresh, btnChatSend;
	
	private final StockDataHolder SDH = new StockDataHolder();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame frame = new MainFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1400, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{431, 0, 89, -70, 36, 85, 0, 0};
		gbl_contentPane.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0};
		gbl_contentPane.columnWeights = new double[]{1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		JLabel lblSymbol = new JLabel("Symbol:");
		GridBagConstraints gbc_lblSymbol = new GridBagConstraints();
		gbc_lblSymbol.anchor = GridBagConstraints.SOUTH;
		gbc_lblSymbol.insets = new Insets(0, 0, 5, 5);
		gbc_lblSymbol.gridx = 0;
		gbc_lblSymbol.gridy = 0;
		contentPane.add(lblSymbol, gbc_lblSymbol);
		
		JLabel lblFetchbutton = new JLabel("FetchButton:");
		GridBagConstraints gbc_lblFetchbutton = new GridBagConstraints();
		gbc_lblFetchbutton.insets = new Insets(0, 0, 5, 5);
		gbc_lblFetchbutton.gridx = 2;
		gbc_lblFetchbutton.gridy = 0;
		contentPane.add(lblFetchbutton, gbc_lblFetchbutton);
		
		JSeparator separator = new JSeparator();
		GridBagConstraints gbc_separator = new GridBagConstraints();
		gbc_separator.gridheight = 6;
		gbc_separator.insets = new Insets(0, 0, 5, 5);
		gbc_separator.gridx = 3;
		gbc_separator.gridy = 0;
		contentPane.add(separator, gbc_separator);
		
		JLabel lblChat = new JLabel("Chat:");
		GridBagConstraints gbc_lblChat = new GridBagConstraints();
		gbc_lblChat.gridwidth = 3;
		gbc_lblChat.insets = new Insets(0, 0, 5, 0);
		gbc_lblChat.gridx = 4;
		gbc_lblChat.gridy = 0;
		contentPane.add(lblChat, gbc_lblChat);
		
		textField_symbol = new JTextField();
		GridBagConstraints gbc_textField_symbol = new GridBagConstraints();
		gbc_textField_symbol.gridwidth = 2;
		gbc_textField_symbol.anchor = GridBagConstraints.NORTH;
		gbc_textField_symbol.insets = new Insets(0, 0, 5, 5);
		gbc_textField_symbol.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_symbol.gridx = 0;
		gbc_textField_symbol.gridy = 1;
		contentPane.add(textField_symbol, gbc_textField_symbol);
		textField_symbol.setColumns(10);
		
		JButton btnFetch = new JButton("Fetch");
		btnFetch.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) { OnFetchButtonClick(); }
		});
		GridBagConstraints gbc_btnFetch = new GridBagConstraints();
		gbc_btnFetch.insets = new Insets(0, 0, 5, 5);
		gbc_btnFetch.gridx = 2;
		gbc_btnFetch.gridy = 1;
		contentPane.add(btnFetch, gbc_btnFetch);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		GridBagConstraints gbc_scrollPane_1 = new GridBagConstraints();
		gbc_scrollPane_1.fill = GridBagConstraints.BOTH;
		gbc_scrollPane_1.gridheight = 5;
		gbc_scrollPane_1.gridwidth = 3;
		gbc_scrollPane_1.insets = new Insets(0, 0, 5, 0);
		gbc_scrollPane_1.gridx = 4;
		gbc_scrollPane_1.gridy = 1;
		contentPane.add(scrollPane_1, gbc_scrollPane_1);
		
		textArea_chat = new JTextArea();
		textArea_chat.setEditable(false);
		scrollPane_1.setViewportView(textArea_chat);
		
		JLabel lblEntriesSavedIn = new JLabel("Entries saved in database:");
		GridBagConstraints gbc_lblEntriesSavedIn = new GridBagConstraints();
		gbc_lblEntriesSavedIn.anchor = GridBagConstraints.SOUTH;
		gbc_lblEntriesSavedIn.insets = new Insets(0, 0, 5, 5);
		gbc_lblEntriesSavedIn.gridx = 0;
		gbc_lblEntriesSavedIn.gridy = 2;
		contentPane.add(lblEntriesSavedIn, gbc_lblEntriesSavedIn);
		
		comboBox_entries = new MyComboBox();
		GridBagConstraints gbc_comboBox_entries = new GridBagConstraints();
		gbc_comboBox_entries.anchor = GridBagConstraints.NORTH;
		gbc_comboBox_entries.insets = new Insets(0, 0, 5, 5);
		gbc_comboBox_entries.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox_entries.gridx = 0;
		gbc_comboBox_entries.gridy = 3;
		contentPane.add(comboBox_entries, gbc_comboBox_entries);
		
		JButton btnGraph = new JButton("Graph");
		btnGraph.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) { OnGraphButtonClick(); }
		});
		
		JButton btnAddEntry = new JButton("Add entry");
		btnAddEntry.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) { OnAddEntryButtonClick();
			}
		});
		GridBagConstraints gbc_btnAddEntry = new GridBagConstraints();
		gbc_btnAddEntry.insets = new Insets(0, 0, 5, 5);
		gbc_btnAddEntry.gridx = 1;
		gbc_btnAddEntry.gridy = 3;
		contentPane.add(btnAddEntry, gbc_btnAddEntry);
		GridBagConstraints gbc_btnGraph = new GridBagConstraints();
		gbc_btnGraph.insets = new Insets(0, 0, 5, 5);
		gbc_btnGraph.gridx = 2;
		gbc_btnGraph.gridy = 3;
		contentPane.add(btnGraph, gbc_btnGraph);
		
		JLabel lblStockPerformance = new JLabel("Stock performance:");
		GridBagConstraints gbc_lblStockPerformance = new GridBagConstraints();
		gbc_lblStockPerformance.gridwidth = 3;
		gbc_lblStockPerformance.insets = new Insets(0, 0, 5, 5);
		gbc_lblStockPerformance.gridx = 0;
		gbc_lblStockPerformance.gridy = 4;
		contentPane.add(lblStockPerformance, gbc_lblStockPerformance);
		
		JScrollPane scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridwidth = 3;
		gbc_scrollPane.insets = new Insets(0, 0, 5, 5);
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 5;
		contentPane.add(scrollPane, gbc_scrollPane);
		
		table_stockInfo = new MyTable();
		table_stockInfo.setDefaultEditor(Object.class, null);
		scrollPane.setViewportView(table_stockInfo);
		
		textField_chatMessage = new JTextField();
		GridBagConstraints gbc_textField_chatMessage = new GridBagConstraints();
		gbc_textField_chatMessage.insets = new Insets(0, 0, 0, 5);
		gbc_textField_chatMessage.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_chatMessage.gridx = 4;
		gbc_textField_chatMessage.gridy = 6;
		contentPane.add(textField_chatMessage, gbc_textField_chatMessage);
		textField_chatMessage.setColumns(10);
		
		btnChatSend = new JButton("Send");
		btnChatSend.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) { OnSendButtonClick(); }
		});
		GridBagConstraints gbc_btnChatSend = new GridBagConstraints();
		gbc_btnChatSend.insets = new Insets(0, 0, 0, 5);
		gbc_btnChatSend.gridx = 5;
		gbc_btnChatSend.gridy = 6;
		contentPane.add(btnChatSend, gbc_btnChatSend);
		
		btnChatRefresh = new JButton("Refresh");
		btnChatRefresh.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) { OnRefreshButtonClick(); }
		});
		GridBagConstraints gbc_btnChatRefresh = new GridBagConstraints();
		gbc_btnChatRefresh.gridx = 6;
		gbc_btnChatRefresh.gridy = 6;
		contentPane.add(btnChatRefresh, gbc_btnChatRefresh);
		
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
		initDataBindings();

        Thread t = new Thread(new Runnable()
        {
            public void run()
            {
            	APIKeysManager.getInstance();
            }
        });
        t.start();
    	
        RefreshChat(false, false);
    	
    	Timer tmr = new Timer();  
    	TimerTask tt = new TimerTask() 
    	{  
    	    @Override  
    	    public void run() 
    	    {  
    	    	if(!repetitiveRefreshLock)
    	    	{
        	    	repetitiveRefreshLock = true;
        	        System.out.println("Auto chat refreshing ...");  
        	        RefreshChat(false, false);
        	    	repetitiveRefreshLock = false;
    	    	}
    	    };  
    	};  
    	tmr.scheduleAtFixedRate(tt,5000,7500);   
	}
	
	static boolean repetitiveRefreshLock = false;
	
	void OnAddEntryButtonClick()
	{
		System.out.println("OnAddEntryButtonClick");
		
		if(comboBox_entries.getSelectedIndex() == -1) JOptionPane.showMessageDialog(this, "Select symbol first!", "No selection", JOptionPane.WARNING_MESSAGE);
		else
		{
			
		
		String provided = JOptionPane.showInputDialog(this, "Enter entry values (separted with commas):\n date, open, high, low, close, adjusted close, volume, dividend amount, split coefficient:");
		
		String provSplit[] = provided.split(",");
		boolean ok = false;
		if(provSplit.length == 9)
		{
			try
			{
				String date = provSplit[0].replaceAll(" ", "");
				new SimpleDateFormat("yyyy-MM-dd").parse(date); //will throw exception if incorrect
				float open = Float.parseFloat(provSplit[1]);
				float high = Float.parseFloat(provSplit[2]);
				float low = Float.parseFloat(provSplit[3]);
				float close = Float.parseFloat(provSplit[4]);
				float adjusted_close = Float.parseFloat(provSplit[5]);
				long volume = Long.parseLong(provSplit[6]);
				float dividend_amount = Float.parseFloat(provSplit[7]);
				float split_coef = Float.parseFloat(provSplit[8]);
				
				ok = true;
				System.out.println("Correct, adding to entries ...");
				
				DailyStockInfoSeries dsis = (DailyStockInfoSeries) comboBox_entries.getSelectedItem();
				
				SingleDayStockInfo sdsi = new SingleDayStockInfo(dsis.getSymbol(), date, open, high, low, close, adjusted_close, volume,
						dividend_amount, split_coef);
				
				SDH.manualAdd(dsis, sdsi);
				comboBox_entries.fireSelectedUpdate();
				
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
				 
		}
		
		if(!ok) JOptionPane.showMessageDialog(this, "Incorrect entry format, please try again", "Incorrect", JOptionPane.WARNING_MESSAGE);
		}
	}
	
	void OnFetchButtonClick()
	{
		System.out.println("OnFetchButtonClick");
		
		if(textField_symbol.getText().length() < 3 || textField_symbol.getText().length() > 15)
			JOptionPane.showMessageDialog(this, "Incorrect symbol length", "Warning", JOptionPane.WARNING_MESSAGE);
		else
		{
			String symbol = textField_symbol.getText();
			DailyStockInfoSeries dsis = DailyStockInfoSeries.parseFromWeb(symbol);
			
			if(dsis != null)
			{
				SDH.add(dsis);
			}
			else
				JOptionPane.showMessageDialog(this, "Data could not be fetched. Please check if provided symbol is correct.", "Fetch failed", JOptionPane.ERROR_MESSAGE);
		}
	}

	void OnGraphButtonClick()
	{
		System.out.println("OnGraphButtonClick");
		DailyStockInfoSeries dsis = (DailyStockInfoSeries) comboBox_entries.getSelectedItem();
		if(dsis != null) ShowChart(dsis);
	}
	
	void OnSendButtonClick()
	{
		System.out.println("OnSendButtonClick");

		final String msg = textField_chatMessage.getText();
		if(msg.length() < 1) JOptionPane.showMessageDialog(this, "You can't send an empty message!", "No message", JOptionPane.ERROR_MESSAGE);
		else
		{
			btnChatSend.setEnabled(false);

	        Thread t = new Thread(new Runnable()
	        {
	            public void run()
	            {
	            	if(SendMessage(msg)) textField_chatMessage.setText("");
	            	RefreshChat(false, true);
	            }
	        });
	        t.start();
		}
	}
	
	void OnRefreshButtonClick()
	{
		System.out.println("OnRefreshButtonClick");
		btnChatRefresh.setEnabled(false);
		RefreshChat(true, false);
	}
	
	void ShowChart(DailyStockInfoSeries dsis)
	{
        Thread t = new Thread(new Runnable()
        {
            public void run()
            {
        		TimeChart chart = TimeChart.getInstance(dsis.getSymbol() + " time series", dsis);         
        		chart.setVisible( true );
            }
        });
        t.start();
	}
	
	String messagesToChatFormat(ArrayList<ChatMessage> messages)
	{
		String f = "";
		Collections.reverse(messages);
		for(ChatMessage m : messages)
		{
			if(RemoteDBManager.loggedInUser != null && m.getSenderName().equals(RemoteDBManager.loggedInUser.getUsername()))
			{
				f += "\t" + m.getDateSent() + ", " + m.getSenderName() + ":\n";
				f += "\t" + m.getText() + "\n\n";
			}
			else
			{
				f += m.getDateSent() + ", " + m.getSenderName() + ":\n";
				f += m.getText() + "\n\n";
			}
		}
		return f;
	}
	
	boolean SendMessage(String messageText)
	{
		if(RemoteDBManager.isInitialized())
		{
			if( RemoteDBManager.AddMessageToDatabase(messageText)) 
			{
				System.out.println("Chat message sent successfully");
				return true;
			}
			else System.out.println("Could not send chat message!");
		}
		else System.out.println("Warning: Message not sent - user is not logged in!");
		
		return false;
	}
	
	void RefreshChat(boolean enableRefreshButtonOnFinish, boolean enableSendButtonOnFinish)
	{
		if(RemoteDBManager.isInitialized())
		{
	        Thread t = new Thread(new Runnable()
	        {
	            public void run()
	            {
	    			ArrayList<ChatMessage> messages = RemoteDBManager.getMessages();
	    			//System.out.println("Messages count: " + messages.size());
	    			if(messages != null)
	    			{
	    				String t = messagesToChatFormat(messages);
	    				//System.out.println("Messages: " + t);
	    				textArea_chat.setText(t);
	    			}
	    			if(enableRefreshButtonOnFinish) btnChatRefresh.setEnabled(true);
	    			if(enableSendButtonOnFinish) btnChatSend.setEnabled(true);
	            }
	        });
	        t.start();
		}
		else
		{
			System.out.println("Warning: Chat not refreshed - RemoteDB is not initialized");
			if(enableRefreshButtonOnFinish) btnChatRefresh.setEnabled(true);
			if(enableSendButtonOnFinish) btnChatSend.setEnabled(true);
		}
	}
	
	protected void initDataBindings() {
		BeanProperty<StockDataHolder, ArrayList<DailyStockInfoSeries>> stockDataHolderBeanProperty = BeanProperty.create("entries");
		BeanProperty<MyComboBox, ArrayList<Object>> myComboBoxBeanProperty = BeanProperty.create("items");
		AutoBinding<StockDataHolder, ArrayList<DailyStockInfoSeries>, MyComboBox, ArrayList<Object>> autoBinding = Bindings.createAutoBinding(UpdateStrategy.READ, SDH, stockDataHolderBeanProperty, comboBox_entries, myComboBoxBeanProperty);
		autoBinding.bind();
		//
		BeanProperty<MyComboBox, Object> myComboBoxBeanProperty_1 = BeanProperty.create("mySelectedItem");
		BeanProperty<MyTable, DefaultTableModel> myTableBeanProperty = BeanProperty.create("dtm");
		AutoBinding<MyComboBox, Object, MyTable, DefaultTableModel> autoBinding_1 = Bindings.createAutoBinding(UpdateStrategy.READ, comboBox_entries, myComboBoxBeanProperty_1, table_stockInfo, myTableBeanProperty);
		autoBinding_1.setConverter(new DSDITableConverter());
		autoBinding_1.bind();
	}
}

