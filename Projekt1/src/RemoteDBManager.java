import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class RemoteDBManager 
{
	static DBConfig config;
	//static final String configPath = "C:\\Users\\Wojciech\\Desktop\\PWr_ProgJava_DBConfig.json";
	//static final String configPath = "C:\\Users\\Wojciech\\Desktop\\PWr_ProgJava_DBConfig_test.json";
	//static final String configPath = "C:\\Users\\Wojciech\\Desktop\\PWr_ProgJava_DBConfig_test_local.json";
	static final String configPath = "C:\\Users\\Wojciech\\Desktop\\PWr_ProgJava_DBConfig_android3cj.json";
	static final String usersTableName = "Users";
	static final String messagesTableName = "Messages";
	
	public static final boolean requireEmailConfirmation = true;
	
	static Connection connection = null;
	
	public static boolean isInitialized() { return (connection != null && config != null); }
	
	public static User loggedInUser;
	
	public static boolean initialize(boolean createTables)
	{
		System.out.println("Loading database config ...");
		config = DBConfig.readFromFile(configPath);
		if(config == null)
		{
			System.out.println("Error: DB config could not be loaded!");
			return false;
		}
		else
		{
			try
		    {
			     System.out.println("Connecting to database...");
			     
			     Class.forName(config.getJDBC_Driver());
			     
			     connection = DriverManager.getConnection(config.getDBUrl(),config.getDB_username(),config.getDB_userpass());
			      
			     System.out.println("Connection established successfully");
			     

				 if(createTables && !tableExists(usersTableName)) createUsersTable();
				 if(createTables && !tableExists(messagesTableName)) createMessagesTable();
				 
			     Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() 
			     {
			         public void run() 
			         {
			             CloseConnection();
			         }
			     }, "Shutdown-thread"));
			     
			     return true;
		    }
		    catch(Exception e)
		    {
			     e.printStackTrace();
			     connection = null;
			     System.out.println("Error: DB connection could not be established!");
		    }
		   
		    
		    return false;
		}
	}
	
	private static boolean tableExists(String tableName)
	{
		ResultSet rs = executeQuery("SELECT count(*) " + 
									"FROM information_schema.TABLES "+ 
									"WHERE (TABLE_SCHEMA = '" + config.getDatabase_Name() + "' AND " + 
									"TABLE_NAME = '" + tableName + "');");
		
		int ct = -1;
		
		try
		{
		    rs.next();
	  		ct = rs.getInt("count(*)");
		}
		catch(Exception e)
		{
			System.out.println("Error: table existence check failed");
			e.printStackTrace();
		}
		
		return (ct == 1);
	}
	
	private static void createUsersTable()
	{
		createTable(usersTableName, getDBTableFormat(User.class));
	}
	
	private static void createMessagesTable()
	{
		createTable(messagesTableName, getDBTableFormat(ChatMessage.class));
	}

	
	private static void createTable(String tableName, String tableFormat)
	{
		if(isInitialized())
		{
			int res = -1;
			
			String upd = "create table " + tableName + "(" + tableFormat + ");";
			
			if(tableExists(tableName))
			{
				System.out.println("Error: Table " + tableName + " already exists!");
			}
			else
			{
				
				try
				{
					res = executeUpdate(upd);
					System.out.println("Table \"" + tableName + "\" created successfully");
				}
				catch(Exception e)
				{
					System.out.println("Error: Failed to create table " + tableName + "!");
					e.printStackTrace();
				}
			}
		}
	}
	
	private static void deleteTable(String tableName)
	{
		if(isInitialized())
		{
			int res = -1;
			
			String upd = "DROP TABLE " + tableName + ";";
			
			try
			{
				res = executeUpdate(upd);
			}
			catch(Exception e)
			{
				System.out.println("Error: Could not delete table \"" + tableName +"\"!");
				e.printStackTrace();
			}
			
			System.out.println("deleteTable \"" + tableName + "\" result: " + res);
		}
	}
	
	public static User getUser(String uname)
	{
		if(isInitialized())
		{
			ResultSet rs = executeQuery("SELECT * FROM " + usersTableName + " WHERE username='" + uname +  "';");
			
			try
			{
				if(rs.next())
				{
					User u = new User(rs.getInt("user_id"), uname, rs.getString("password"), "", rs.getString("email"), rs.getString("activationCode"), rs.getInt("activated"));
					return u;
				}
			}
			catch(Exception e)
			{
				System.out.println("Error: getUser failed");
				e.printStackTrace();
			}
		}
		
		return null;
	}
	
	public static ArrayList<ChatMessage> getMessages()
	{
		if(isInitialized())
		{
			ResultSet rs = executeQuery("SELECT * FROM " + messagesTableName +  ";");
			ArrayList<ChatMessage> list = new ArrayList<ChatMessage>();
			
			try
			{
				while(rs.next())
				{
					int _message_id = rs.getInt("message_id");
					String _senderName = rs.getString("senderName");
					String _dateSent = rs.getString("dateSent");
					String _text = rs.getString("text");
					ChatMessage mes = new ChatMessage(_message_id, _senderName, _dateSent, _text);
					list.add(mes);
				}
				
				return list;
			}
			catch(Exception e)
			{
				System.out.println("Error: getMessages failed");
				e.printStackTrace();
			}
		}
		
		return null;
	}
	
	public static boolean isEmailUsed(String emailToCheck)
	{
		if(isInitialized())
		{
			ResultSet rs = executeQuery("SELECT * FROM " + usersTableName + " WHERE email='" + emailToCheck +  "';");
			
			try
			{
				if(rs.next()) return true;
			}
			catch(Exception e)
			{
				System.out.println("Error: isEmailUsed failed");
				e.printStackTrace();
			}
		}
		
		return false;
	}
	
	public static int getNewUserId()
	{
		if(isInitialized())
		{
			ResultSet rs = executeQuery("SELECT user_id FROM " + usersTableName + " ORDER BY user_id DESC;");
			
			try
			{
				if(rs.next())
				{
					int highest_id = rs.getInt("user_id");
					return highest_id + 1;
				}
				
				return 0;
			}
			catch(Exception e)
			{
				System.out.println("Error: getNewUserId failed");
				e.printStackTrace();
			}
		}
		
		return -1;
	}
	
	public static int getNewMessageId()
	{
		if(isInitialized())
		{
			ResultSet rs = executeQuery("SELECT message_id FROM " + messagesTableName + " ORDER BY message_id DESC;");
			
			try
			{
				if(rs.next())
				{
					int highest_id = rs.getInt("message_id");
					return highest_id + 1;
				}
				
				return 0;
			}
			catch(Exception e)
			{
				System.out.println("Error: getNewMessageId failed");
				e.printStackTrace();
			}
		}
		
		return -1;
	}
	
	public static boolean AddUserToDatabase(User u)
	{
		if(u.getUser_id() == -1) return false;
		boolean success = AddEntryToDatabase(u, usersTableName);
		if(success) System.out.println("Username " + u.getUsername() + " added to database.");
		return success;
	}
	
	public static boolean ActivateUser(String username)
	{
		if(!isInitialized() || !tableExists(usersTableName) || getUser(username) == null) return false;
		
		int res = -1;
		
		String upd = "UPDATE " + usersTableName + " SET activated = 1 WHERE username='" + username + "'";
		
		try
		{
			res = executeUpdate(upd);
			if(res != Statement.EXECUTE_FAILED) return true;
		}
		catch(Exception e)
		{
			System.out.println("Error: ActivateUser failed!");
			e.printStackTrace();
		}
		
		
		return false;
	}
	
	public static boolean DebugAddMessageToDatabase(ChatMessage c)
	{
		if(!isInitialized()) return false;
		
		boolean success = AddEntryToDatabase(c, messagesTableName);
		if(success) System.out.println("Message '" + (c.getText().length() <= 15 ? c.getText() : c.getText().substring(0, 14) + "...") + "' added to database.");
		return success;
	}
	
	public static boolean AddMessageToDatabase(String messageText)
	{
		if(!isInitialized() || loggedInUser == null) return false;
		
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
		
		ChatMessage c = new ChatMessage(getNewMessageId(), loggedInUser.getUsername(), formatter.format(date), messageText);
		
		boolean success = AddEntryToDatabase(c, messagesTableName);
		if(success) System.out.println("Message '" + (c.getText().length() <= 15 ? c.getText() : c.getText().substring(0, 14) + "...") + "' added to database.");
		return success;
	}
	
	private static boolean AddEntryToDatabase(DBEntry obj, String tableName)
	{
		if(!isInitialized() || !tableExists(tableName)) return false;
		

		int res = -1;
		
		String upd = "INSERT INTO " + tableName + " VALUES (" + obj.toDatabaseEntryFormat() + ");";
		
		try
		{
			res = executeUpdate(upd);
			if(res != Statement.EXECUTE_FAILED) return true;
		}
		catch(Exception e)
		{
			System.out.println("Error: addEntryToDatabase failed!");
			e.printStackTrace();
		}
		
		
		return false;
	}
	
	
	private static int executeUpdate(String upd)
	{
		if(!isInitialized()) return Statement.EXECUTE_FAILED;
		Statement xstmt;
		int xrs = -1;
		try
		{
			xstmt = connection.createStatement();
			xrs = xstmt.executeUpdate(upd);
			
			xstmt.close();
		}
		catch(Exception e)
		{
			System.out.println("Error: executeUpdate failed");
			e.printStackTrace();
		}
		
		return xrs;
	}
	
	private static ResultSet executeQuery(String qry)
	{
		if(!isInitialized()) return null;
		Statement stmt;
		ResultSet rs;
		try
		{
		      stmt = connection.createStatement();
		      rs = stmt.executeQuery(qry);
		}
		catch (Exception e)
		{
			System.out.println("Error: executeQuery failed");
			e.printStackTrace();
			return null;
		}
		
		return rs;
	}
	
	public static ArrayList<String> getTablesList()
	{
		if(isInitialized())
		{
			ArrayList<String> tablesList = new ArrayList<String>();
			ResultSet rs = executeQuery("show tables;");
			
			try
			{
				while(rs.next())
				{
					String tname = rs.getString("Tables_in_" + config.getDatabase_Name().toLowerCase());
					tablesList.add(tname);
				}
				
				return tablesList;
			}
			catch(Exception e)
			{
				System.out.println("Error: getTablesList failed");
				e.printStackTrace();
			}
		}
		
		return null;
	}
	
	public static String getDBTableFormat(Class c)
	{
		Field[] fields = c.getDeclaredFields();
		String dbTF = "";
		for(Field f : fields)
			dbTF += f.getName() + " " + RemoteDBManager.getSQLDataType(f.getType().getSimpleName()) + ", ";
		
		dbTF = dbTF.substring(0, dbTF.length() - 2);
		
		return dbTF;
	}
	
	public static String getSQLDataType(String typeName)
	{
		if(typeName.toLowerCase().contains("float") ||
		   typeName.toLowerCase().contains("double"))
			return "float";
			

		if(typeName.toLowerCase().contains("int"))
			return "int";
		
		return "varchar (255)";
	}
	
	public static void ClearOutDatabase()
	{
		initialize(false);
		ArrayList<String> tables = getTablesList();
		
		System.out.println("Database clearing process starting ...");
		
		for(String tableName : tables)
			deleteTable(tableName);
		
		System.out.println("Database clearing process finished");
		CloseConnection();
	}
	
	public static void CloseConnection()
	{
		try 
		{
			if(connection != null && !connection.isClosed()) 
			{
				System.out.println("Closing connection...");
				connection.close();
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
	}
}
