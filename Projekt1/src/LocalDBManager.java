import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class LocalDBManager 
{
	static final String databasePath = "D:/Users/Wojciech/Documents/PWr/ProgJava/projekt1.db";
	static final String dataTableName = "Data";
	static Connection connection;
	
	public static boolean isInitialized() { return (connection != null); }
	
	public static boolean initialize()
	{
		try
	    {
		     System.out.println("Connecting to local database...");
		     
		     Class.forName("org.sqlite.JDBC");
		     
		     connection = DriverManager.getConnection("jdbc:sqlite:" + databasePath);
		     
		     if(connection != null)
		     {
			     System.out.println("Connection to LocalDB established successfully");
			     
			     if(!tableExists(dataTableName)) createDataTable();
		     }
		      
		     
		     return true;
	    }
	    catch(Exception e)
	    {
		     e.printStackTrace();
		     connection = null;
		     System.out.println("Error: LocalDB connection could not be established!");
	    }
	   
	    return false;
	}
	
	private static boolean tableExists(String tableName)
	{
		ResultSet rs = executeQuery("SELECT name FROM sqlite_master WHERE (type = \"table\" AND NAME = \"" + tableName + "\")");
		
		int ct = -1;
		
		try
		{
			if(rs.next()) return true;
			return false;
		}
		catch(Exception e)
		{
			System.out.println("Error: table existence check failed");
			e.printStackTrace();
		}
		
		return false;
	}

	private static void createDataTable()
	{
		createTable(dataTableName, SingleDayStockInfo.getDBTableColumnHeaders());
	}
	
	private static void createTable(String tableName, String tableFormat)
	{
		if(isInitialized())
		{
			int res = -1;
			
			String upd = "create table " + tableName + "(" + tableFormat + ");";
			
			if(tableExists(tableName))
			{
				System.out.println("Error: Table " + tableName + " already exists!");
			}
			else
			{
				
				try
				{
					res = executeUpdate(upd);
					System.out.println("Table \"" + tableName + "\" created successfully");
				}
				catch(Exception e)
				{
					System.out.println("Error: Failed to create table " + tableName + "!");
					e.printStackTrace();
				}
			}
		}
	}

    public static boolean AddSDSIToDatabase(SingleDayStockInfo sdsi)
    {
        boolean success = AddEntryToDatabase(sdsi, dataTableName);
        return success;
    }

    private static boolean AddEntryToDatabase(DBEntry obj, String tableName)
    {
        if(!isInitialized() || !tableExists(tableName)) return false;


        int res = -1;

        String upd = "INSERT INTO " + tableName + " VALUES (" + obj.toDatabaseEntryFormat() + ");";

        try
        {
            res = executeUpdate(upd);
            if(res != Statement.EXECUTE_FAILED) return true;
        }
        catch(Exception e)
        {
            System.out.println("Error: addEntryToDatabase failed!");
            e.printStackTrace();
        }


        return false;
    }
	
	
	private static int executeUpdate(String upd)
	{
		if(!isInitialized()) return Statement.EXECUTE_FAILED;
		Statement xstmt;
		int xrs = -1;
		try
		{
			xstmt = connection.createStatement();
			xrs = xstmt.executeUpdate(upd);
			
			xstmt.close();
		}
		catch(Exception e)
		{
			System.out.println("Error: executeUpdate failed");
			e.printStackTrace();
		}
		
		return xrs;
	}
	
	private static ResultSet executeQuery(String qry)
	{
		if(!isInitialized()) return null;
		Statement stmt;
		ResultSet rs;
		try
		{
		      stmt = connection.createStatement();
		      rs = stmt.executeQuery(qry);
		}
		catch (Exception e)
		{
			System.out.println("Error: executeQuery failed");
			e.printStackTrace();
			return null;
		}
		
		return rs;
	}
	
	public static ArrayList<SingleDayStockInfo> getSDSIs(String symbol)
	{
		if(isInitialized())
		{
			ResultSet rs;
		 	if(symbol != null) rs = executeQuery("SELECT * FROM " + dataTableName + " WHERE symbol='" + symbol +  "';");
		 	else  rs = executeQuery("SELECT * FROM " + dataTableName + ";");
			ArrayList<SingleDayStockInfo> list = new ArrayList<SingleDayStockInfo>();
			
			try
			{
				while(rs.next())
				{
					String _symbol = rs.getString("symbol");
					String date = rs.getString("date");
					float open = rs.getFloat("open"); 
					float high = rs.getFloat("high");
					float low = rs.getFloat("low");
					float close = rs.getFloat("close");
					float adjusted_close = rs.getFloat("adjusted_close");
					long volume = rs.getLong("volume");
					float dividend_amount = rs.getFloat("dividend_amount");
					float split_coef = rs.getFloat("split_coef");
					
					SingleDayStockInfo sdsi = new SingleDayStockInfo(_symbol, date, open, high, low, close, adjusted_close, volume, dividend_amount, split_coef);
					list.add(sdsi);
				}
				
				return list;
			}
			catch(Exception e)
			{
				System.out.println("Error: getSDSIs failed");
				e.printStackTrace();
			}
		}
		
		return null;
	}
	
	public static ArrayList<DailyStockInfoSeries> LoadDSISs()
	{
		System.out.println("LocalDBManager: Loading entries from database ...");
		
		ArrayList<DailyStockInfoSeries> DSISs = new ArrayList<DailyStockInfoSeries>();
		ArrayList<SingleDayStockInfo> allEntries = getSDSIs(null);
		
		if(allEntries != null)
		{
			for(SingleDayStockInfo sdsi : allEntries)
			{
				DailyStockInfoSeries parent = null;
				for(DailyStockInfoSeries potentialParent : DSISs)
				{
					if(potentialParent.getSymbol().equals(sdsi.getSymbol()))
					{
						parent = potentialParent;
						break;
					}
				}
				
				if(parent != null)
				{
					parent.add(sdsi);
				}
				else
				{
					parent = new DailyStockInfoSeries();
					parent.setSymbol(sdsi.getSymbol());
					parent.add(sdsi);
					DSISs.add(parent);
				}
			}
			
			System.out.println(DSISs.size() + " DSISs containing total of " + allEntries.size() +  " entries loaded from local database");
		}
		

		
		return DSISs;
	}
	
	public static ArrayList<String> getTablesList()
	{
		if(isInitialized())
		{
			ArrayList<String> tablesList = new ArrayList<String>();
			ResultSet rs = executeQuery("SELECT name FROM sqlite_master WHERE type = \"table\"");
			
			try
			{
				while(rs.next())
				{
					String tname = rs.getString("name");
					tablesList.add(tname);
				}
				
				return tablesList;
			}
			catch(Exception e)
			{
				System.out.println("Error: getTablesList failed");
				e.printStackTrace();
			}
		}
		
		return null;
	}
	
	public static void main(String[] args) 
	{
		initialize();
		ArrayList<String> tnames = getTablesList();
		for(String s : tnames)
		{
			System.out.println(s);
		}
	}
}
