import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

public class SDSIComparator implements Comparator<SingleDayStockInfo> 
{
    @Override
    public int compare(SingleDayStockInfo a, SingleDayStockInfo b) 
    {
    	try
    	{
        	Date dateA = new SimpleDateFormat("yyyy-MM-dd").parse(a.getDate());
        	Date dateB = new SimpleDateFormat("yyyy-MM-dd").parse(b.getDate());
        	
        	return dateA.compareTo(dateB);
    	}
    	catch (Exception e)
    	{
    		e.printStackTrace();
    	}
    	
    	return 0;
    }
}