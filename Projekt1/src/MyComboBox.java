import java.util.ArrayList;

import javax.swing.JComboBox;

public class MyComboBox extends JComboBox
{
	private ArrayList<Object> items = new ArrayList<Object>();
	private Object mySelectedItem = null;

	public ArrayList<Object> getItems() 
	{
		return items;
	}
	
	public void setItems(ArrayList<Object> items) 
	{
		ArrayList<Object> oldItems = this.items;
		System.out.println("Setting " + items.size() + " items ....");
		
		int selectedIndex = getSelectedIndex();
		
		super.removeAllItems();
		for(Object o : items) super.addItem(o);

		ArrayList<Object> newItems = getAllOriginalItems();
		this.items = newItems;
		firePropertyChange("items", oldItems, newItems);
		
		setSelectedIndex(selectedIndex);
		
	}

//	public void setItems(ArrayList<Object> items) 
//	{
//		int counter = 0;
//		System.out.println("Set items ....... ");
//		
//		ArrayList<Object> oldItems = this.items;
//		
//		for(Object n : items)
//		{
//			boolean wasOnList = false;
//			for(Object org : oldItems) if(org == n) wasOnList = true;
//			
//			if(!wasOnList)
//			{
//				counter++;
//				super.addItem(n);
//			}
//		}
//
//		ArrayList<Object> newItems = getAllOriginalItems();
//		this.items = newItems;
//		firePropertyChange("items", oldItems, newItems);
//		System.out.println(counter + " items were not on ComboBox list, and were added");
//		
//	}

	public Object getMySelectedItem() {
		return mySelectedItem;
	}
	
	public void fireSelectedUpdate()
	{
		int sel = getSelectedIndex();
		setSelectedIndex(-1);
		setSelectedIndex(sel);
		//firePropertyChange("mySelectedItem", getSelectedItem(), getSelectedItem());
	}

	@Override
	public void setSelectedItem(Object arg0) 
	{
		Object oldmSI = mySelectedItem;
		super.setSelectedItem(arg0);
		Object newmSI = getSelectedItem();
		this.mySelectedItem = newmSI;
		firePropertyChange("mySelectedItem", oldmSI, newmSI);
		//System.out.println("selected item changed");
	}

	public ArrayList<Object> getAllOriginalItems() 
	{
		ArrayList<Object> oitems = new ArrayList<Object>();
		int ct = getItemCount();
		for(int i = 0; i < ct; i++) oitems.add(getItemAt(i));
		return oitems;
	}

	@Override
	public void addItem(Object item) 
	{
		if(Contains(item)) System.out.println("MyComboBox: Iteam already listed!");
		else
		{
			ArrayList<Object> oldItems = this.items;
			super.addItem(item);
			ArrayList<Object> newItems = getAllOriginalItems();
			this.items = newItems;
			firePropertyChange("items", oldItems, newItems);
			
			if(getItemCount() == 1)
			{
				Object oldmSI = mySelectedItem;
				Object newmSI = item;
				this.mySelectedItem = newmSI;
				firePropertyChange("mySelectedItem", oldmSI, newmSI);
				System.out.println("selected item changed");
			}
		}
	}

	@Override
	public void removeAllItems() 
	{
		ArrayList<Object> oldItems = this.items;
		super.removeAllItems();
		ArrayList<Object> newItems = getAllOriginalItems();
		this.items = newItems;
		firePropertyChange("items", oldItems, newItems);
	}

	@Override
	public void removeItem(Object anObject) 
	{
		ArrayList<Object> oldItems = this.items;
		super.removeItem(anObject);
		ArrayList<Object> newItems = getAllOriginalItems();
		this.items = newItems;
		firePropertyChange("items", oldItems, newItems);
	}

	@Override
	public void removeItemAt(int anIndex) 
	{
		ArrayList<Object> oldItems = this.items;
		super.removeItemAt(anIndex);
		ArrayList<Object> newItems = getAllOriginalItems();
		this.items = newItems;
		firePropertyChange("items", oldItems, newItems);
	}
	
	private boolean Contains(Object o)
	{
		ArrayList<Object> currentItems = getAllOriginalItems();
		
		for(Object current : currentItems) if(current == o) return true;

		return false;
	}

	private java.beans.PropertyChangeSupport propertyChangeSupport;

	public void addPropertyChangeListener(java.beans.PropertyChangeListener listener) 
	{
		if(propertyChangeSupport == null) propertyChangeSupport = new java.beans.PropertyChangeSupport(this);
		propertyChangeSupport.addPropertyChangeListener(listener);
	}

	public void addPropertyChangeListener(String propertyName, java.beans.PropertyChangeListener listener) 
	{
		if(propertyChangeSupport == null) propertyChangeSupport = new java.beans.PropertyChangeSupport(this);
		propertyChangeSupport.addPropertyChangeListener(propertyName, listener);
	}

	public void removePropertyChangeListener(java.beans.PropertyChangeListener listener) 
	{
		if(propertyChangeSupport == null) propertyChangeSupport = new java.beans.PropertyChangeSupport(this);
		propertyChangeSupport.removePropertyChangeListener(listener);
	}

	public void removePropertyChangeListener(String propertyName, java.beans.PropertyChangeListener listener) 
	{
		if(propertyChangeSupport == null) propertyChangeSupport = new java.beans.PropertyChangeSupport(this);
		propertyChangeSupport.removePropertyChangeListener(propertyName, listener);
	}

	protected void firePropertyChange(String propertyName, Object oldValue, Object newValue) 
	{
		if(propertyChangeSupport == null) propertyChangeSupport = new java.beans.PropertyChangeSupport(this);
		propertyChangeSupport.firePropertyChange(propertyName, oldValue, newValue);
	}
	
}
