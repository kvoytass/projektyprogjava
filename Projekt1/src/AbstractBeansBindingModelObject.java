public abstract class AbstractBeansBindingModelObject 
{
	private java.beans.PropertyChangeSupport propertyChangeSupport;// = new java.beans.PropertyChangeSupport(this);

	public void addPropertyChangeListener(java.beans.PropertyChangeListener listener) 
	{
		if(propertyChangeSupport == null) propertyChangeSupport = new java.beans.PropertyChangeSupport(this);
		propertyChangeSupport.addPropertyChangeListener(listener);
	}

	public void addPropertyChangeListener(String propertyName, java.beans.PropertyChangeListener listener) 
	{
		if(propertyChangeSupport == null) propertyChangeSupport = new java.beans.PropertyChangeSupport(this);
		propertyChangeSupport.addPropertyChangeListener(propertyName, listener);
	}

	public void removePropertyChangeListener(java.beans.PropertyChangeListener listener) 
	{
		if(propertyChangeSupport == null) propertyChangeSupport = new java.beans.PropertyChangeSupport(this);
		propertyChangeSupport.removePropertyChangeListener(listener);
	}

	public void removePropertyChangeListener(String propertyName, java.beans.PropertyChangeListener listener) 
	{
		if(propertyChangeSupport == null) propertyChangeSupport = new java.beans.PropertyChangeSupport(this);
		propertyChangeSupport.removePropertyChangeListener(propertyName, listener);
	}

	protected void firePropertyChange(String propertyName, Object oldValue, Object newValue) 
	{
		if(propertyChangeSupport == null) propertyChangeSupport = new java.beans.PropertyChangeSupport(this);
		propertyChangeSupport.firePropertyChange(propertyName, oldValue, newValue);
	}
}