import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SingleDayStockInfo implements DBEntry
{
	String symbol, date;
	float open, high, low, close, adjusted_close;
	long volume;
	float dividend_amount, split_coef;
	
	public SingleDayStockInfo(String symbol, String date, float open, float high, float low, float close, float adjusted_close, long volume,
			float dividend_amount, float split_coef) {
		super();
		this.symbol = symbol;
		this.date = date;
		this.open = open;
		this.high = high;
		this.low = low;
		this.close = close;
		this.adjusted_close = adjusted_close;
		this.volume = volume;
		this.dividend_amount = dividend_amount;
		this.split_coef = split_coef;
	}
	
	public SingleDayStockInfo()
	{
		
	}
	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public float getOpen() {
		return open;
	}

	public void setOpen(float open) {
		this.open = open;
	}

	public float getHigh() {
		return high;
	}

	public void setHigh(float high) {
		this.high = high;
	}

	public float getLow() {
		return low;
	}

	public void setLow(float low) {
		this.low = low;
	}

	public float getClose() {
		return close;
	}

	public void setClose(float close) {
		this.close = close;
	}

	public float getAdjusted_close() {
		return adjusted_close;
	}

	public void setAdjusted_close(float adjusted_close) {
		this.adjusted_close = adjusted_close;
	}

	public long getVolume() {
		return volume;
	}

	public void setVolume(long volume) {
		this.volume = volume;
	}

	public float getDividend_amount() {
		return dividend_amount;
	}

	public void setDividend_amount(float dividend_amount) {
		this.dividend_amount = dividend_amount;
	}

	public float getSplit_coef() {
		return split_coef;
	}

	public void setSplit_coef(float split_coef) {
		this.split_coef = split_coef;
	}

	public void print()
	{
		String inf = "";
		inf += "=======================\n";
		inf += "Data from " + date + "\n";
		inf += "Symbol: " + symbol + "\n";
		inf += "Open: " + open + "\n";
		inf += "High: " + high + "\n";
		inf += "Low: " + low + "\n";
		inf += "Close: " + close + "\n";
		inf += "Adjsuted close: " + adjusted_close + "\n";
		inf += "Volume: " + volume + "\n";
		inf += "Dividend amount: " + dividend_amount + "\n";
		inf += "Split coefficient: " + split_coef + "\n";
		inf += "=======================\n";
		
		System.out.print(inf);
	}

	public boolean equals(SingleDayStockInfo other) 
	{
		return this.getDate().equals(other.getDate());
	}
	
	public Date getDateObject()
	{
		Date d;
		try 
		{
			d = new SimpleDateFormat("yyyy-MM-dd").parse(this.getDate());
			return d;
		} 
		catch (ParseException e) 
		{
			e.printStackTrace();
		}
		
		return null;
	}

	public static String getDBTableColumnHeaders()
	{
        String tr = "";
        tr += "symbol varchar(16), ";
        tr += "date varchar(16), ";
        tr += "open float, ";
        tr += "high float, ";
        tr += "low float, ";
        tr += "close float, ";
        tr += "adjusted_close float, ";
        tr += "volume int, ";
        tr += "dividend_amount float, ";
        tr += "split_coef float";

        return tr;
	}

	@Override
	public String toDatabaseEntryFormat() 
	{
        String tr = "";
        tr += "'" + symbol + "', ";
        tr += "'" + date + "', ";
        tr += open + ", ";
        tr += high + ", ";
        tr += low + ", ";
        tr += close + ", ";
        tr += adjusted_close + ", ";
        tr += volume + ", ";
        tr += dividend_amount + ", ";
        tr += split_coef;

        return tr;
	}
	
}
