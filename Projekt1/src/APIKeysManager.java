import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Random;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class APIKeysManager 
{
	private ArrayList<String> apiKeys;
	private transient int lastUsedPos = -1;
	private static final String keyFilePath = "C:\\Users\\Wojciech\\Desktop\\PWr_ProgJava_APIKeys.json";
	
	private static APIKeysManager instance;
	public static APIKeysManager getInstance()
	{
		return (instance != null ? instance : readFromFile() );
	}
	
//	public static void writeToFile()
//	{
//		if(instance != null)
//		{
//	        FileOutputStream outputStream;
//	        try 
//	        {
//	    		Gson gson = new GsonBuilder().setPrettyPrinting().create();
//	    		
//	    		String json = gson.toJson(instance);
//	        	
//	            outputStream = new FileOutputStream(new File(keyFilePath));
//	            outputStream.write(json.getBytes());
//	            outputStream.close();
//	        } 
//	        catch (Exception e) 
//	        {
//	            e.printStackTrace();
//	        }
//		}
//	}
	
//	public static void specialWriteToFile()
//	{
//		APIKeysManager mak = new APIKeysManager();
//		mak.apiKeys = new ArrayList<String>();
//		mak.apiKeys.add(""); //add keys here
//		mak.apiKeys.add("");
//		mak.apiKeys.add("");
//		mak.apiKeys.add("");
//		mak.apiKeys.add("");
//		
//        FileOutputStream outputStream;
//        try 
//        {
//    		Gson gson = new GsonBuilder().setPrettyPrinting().create();
//    		
//    		String json = gson.toJson(mak);
//        	
//            outputStream = new FileOutputStream(new File(keyFilePath));
//            outputStream.write(json.getBytes());
//            outputStream.close();
//        } 
//        catch (Exception e) 
//        {
//            e.printStackTrace();
//        }
//	}
	
	public String getKey()
	{
		if(this.apiKeys == null) readFromFile();
		if(this.apiKeys == null || apiKeys.size() < 1) return null;
		
		
		if(lastUsedPos == -1)
		{
			Random rnd = new Random();
			lastUsedPos = rnd.nextInt(apiKeys.size());
		}
		
		int pos;
		if(lastUsedPos >= apiKeys.size() - 1) pos = 0;
		else pos = lastUsedPos + 1;
		lastUsedPos = pos;
		
		return apiKeys.get(pos);
	}
	
	private static APIKeysManager readFromFile()
	{
		File file = new File(keyFilePath);
		try 
		{
			System.out.println("Reading API Keys from file ...");
			java.io.FileInputStream inputStream = new java.io.FileInputStream(file);
			byte[] _read = new byte[1024];
			int l = inputStream.read(_read);
			String s_read = new String(_read);
			s_read = s_read.substring(0, l);
			
			Gson gson = new GsonBuilder().setLenient().create();
			try
			{
				APIKeysManager a = gson.fromJson(s_read, APIKeysManager.class);
				System.out.println("API Keys read from file");
				instance = a;
				return a;
			}
			catch(Exception e)
			{
				System.out.println("Failed to read API Keys from file");
				e.printStackTrace();
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return null;
	}
}
