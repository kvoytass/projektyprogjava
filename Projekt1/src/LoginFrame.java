import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridBagLayout;
import javax.swing.JButton;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class LoginFrame extends JFrame 
{
	public static LoginFrame instance;

	private JPanel contentPane;
	private JTextField loginField;
	private JPasswordField passwordField;
	private JPasswordField passwordFieldConfirm;
	private JTextField emailField;
	
	private JLabel lblConfirmPassword;
	private JLabel lblEmail;
	private JButton btnConfirm;

	public LoginFrame() 
	{
		instance = this;
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 220);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{0, 0, 0};
		gbl_contentPane.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0};
		gbl_contentPane.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		JButton btnLogin = new JButton("Login");
		btnLogin.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				DisplayLoginVersion();
			}
		});
		GridBagConstraints gbc_btnLogin = new GridBagConstraints();
		gbc_btnLogin.insets = new Insets(0, 0, 5, 5);
		gbc_btnLogin.gridx = 0;
		gbc_btnLogin.gridy = 0;
		contentPane.add(btnLogin, gbc_btnLogin);
		
		JButton btnRegister = new JButton("Register");
		btnRegister.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				DisplayRegisterVersion();
			}
		});
		GridBagConstraints gbc_btnRegister = new GridBagConstraints();
		gbc_btnRegister.insets = new Insets(0, 0, 5, 0);
		gbc_btnRegister.anchor = GridBagConstraints.WEST;
		gbc_btnRegister.gridx = 1;
		gbc_btnRegister.gridy = 0;
		contentPane.add(btnRegister, gbc_btnRegister);
		
		JLabel lblLogin = new JLabel("Login");
		GridBagConstraints gbc_lblLogin = new GridBagConstraints();
		gbc_lblLogin.anchor = GridBagConstraints.EAST;
		gbc_lblLogin.insets = new Insets(0, 0, 5, 5);
		gbc_lblLogin.gridx = 0;
		gbc_lblLogin.gridy = 1;
		contentPane.add(lblLogin, gbc_lblLogin);
		
		loginField = new JTextField();
		GridBagConstraints gbc_loginField = new GridBagConstraints();
		gbc_loginField.insets = new Insets(0, 0, 5, 0);
		gbc_loginField.fill = GridBagConstraints.HORIZONTAL;
		gbc_loginField.gridx = 1;
		gbc_loginField.gridy = 1;
		contentPane.add(loginField, gbc_loginField);
		loginField.setColumns(10);
		
		JLabel lblPassword = new JLabel("Password");
		GridBagConstraints gbc_lblPassword = new GridBagConstraints();
		gbc_lblPassword.anchor = GridBagConstraints.EAST;
		gbc_lblPassword.insets = new Insets(0, 0, 5, 5);
		gbc_lblPassword.gridx = 0;
		gbc_lblPassword.gridy = 2;
		contentPane.add(lblPassword, gbc_lblPassword);
		
		passwordField = new JPasswordField();
		GridBagConstraints gbc_passwordField = new GridBagConstraints();
		gbc_passwordField.insets = new Insets(0, 0, 5, 0);
		gbc_passwordField.fill = GridBagConstraints.HORIZONTAL;
		gbc_passwordField.gridx = 1;
		gbc_passwordField.gridy = 2;
		contentPane.add(passwordField, gbc_passwordField);
		
		lblConfirmPassword = new JLabel("Confirm password");
		GridBagConstraints gbc_lblConfirmPassword = new GridBagConstraints();
		gbc_lblConfirmPassword.anchor = GridBagConstraints.EAST;
		gbc_lblConfirmPassword.insets = new Insets(0, 0, 5, 5);
		gbc_lblConfirmPassword.gridx = 0;
		gbc_lblConfirmPassword.gridy = 3;
		contentPane.add(lblConfirmPassword, gbc_lblConfirmPassword);
		
		passwordFieldConfirm = new JPasswordField();
		GridBagConstraints gbc_passwordFieldConfirm = new GridBagConstraints();
		gbc_passwordFieldConfirm.insets = new Insets(0, 0, 5, 0);
		gbc_passwordFieldConfirm.fill = GridBagConstraints.HORIZONTAL;
		gbc_passwordFieldConfirm.gridx = 1;
		gbc_passwordFieldConfirm.gridy = 3;
		contentPane.add(passwordFieldConfirm, gbc_passwordFieldConfirm);
		
		lblEmail = new JLabel("Email");
		GridBagConstraints gbc_lblEmail = new GridBagConstraints();
		gbc_lblEmail.anchor = GridBagConstraints.EAST;
		gbc_lblEmail.insets = new Insets(0, 0, 5, 5);
		gbc_lblEmail.gridx = 0;
		gbc_lblEmail.gridy = 4;
		contentPane.add(lblEmail, gbc_lblEmail);
		
		emailField = new JTextField();
		GridBagConstraints gbc_emailField = new GridBagConstraints();
		gbc_emailField.insets = new Insets(0, 0, 5, 0);
		gbc_emailField.fill = GridBagConstraints.HORIZONTAL;
		gbc_emailField.gridx = 1;
		gbc_emailField.gridy = 4;
		contentPane.add(emailField, gbc_emailField);
		emailField.setColumns(10);
		
		btnConfirm = new JButton("Register");
		btnConfirm.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) 
			{
				OnConfirmButtonClick();
			}
		});
		btnConfirm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		GridBagConstraints gbc_btnConfirm = new GridBagConstraints();
		gbc_btnConfirm.gridwidth = 2;
		gbc_btnConfirm.gridx = 0;
		gbc_btnConfirm.gridy = 5;
		contentPane.add(btnConfirm, gbc_btnConfirm);
		
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
		DisplayLoginVersion();
	}
	
	void DisplayLoginVersion()
	{
		lblConfirmPassword.setVisible(false);
		lblEmail.setVisible(false);
		passwordFieldConfirm.setVisible(false);
		emailField.setVisible(false);
		btnConfirm.setText("LOGIN");
	}
	
	void DisplayRegisterVersion()
	{
		lblConfirmPassword.setVisible(true);
		lblEmail.setVisible(true);
		passwordFieldConfirm.setVisible(true);
		emailField.setVisible(true);
		btnConfirm.setText("REGISTER");
	}
	
	void OnConfirmButtonClick()
	{
		if(btnConfirm.getText() == "LOGIN") OnLoginAttempt();
		else OnRegisterAttempt();
	}
	
	void OnLoginAttempt()
	{
		if(loginField.getText().length() < 1) JOptionPane.showMessageDialog(this, "Please enter login", "No login", JOptionPane.ERROR_MESSAGE);
		else if(passwordField.getPassword().length < 1)  JOptionPane.showMessageDialog(this, "Please enter password", "No password", JOptionPane.ERROR_MESSAGE);
		else
		{
			String login = loginField.getText();
			User u = RemoteDBManager.getUser(login);
			if(u == null) //user does not exist
			{
				JOptionPane.showMessageDialog(this, "Provided username or password is wrong", "Wrong username or password", JOptionPane.ERROR_MESSAGE);
			}
			else
			{
				String fieldPass = new String(passwordField.getPassword());
				if(Hasher.getHashed(fieldPass).equals(u.getPassword())) //correct pass
				{
					if(!RemoteDBManager.requireEmailConfirmation || u.getActivated() == 1) //user is activated, or not required
					{
						String info = "User " + login + " successfully logged in! :)";
						JOptionPane.showMessageDialog(this, info);
						System.out.println(info);
						
						OnLoginOrRegisterSuccess(u);
					}
					else
					{
						String answer = "";
						Thread t = new Thread(new Runnable() {
						    public void run() {
						    	Mailer.SendEmail(RemoteDBManager.config, u.getEmail(), "WK Services App - Activation Code", "Your activation code is: " + u.getActivationCode());
						    }
						});
						t.start();
						answer = JOptionPane.showInputDialog(this, "Your account has not been activated yet.\nWe've sent a new email with activation code.\nPlease enter activation code sent to your email:");
						while(!answer.contentEquals(u.getActivationCode()))
						{
							answer = JOptionPane.showInputDialog(this, "Wrong activtion code!\nPlease enter activation code sent to your email:");
						}
						
						RemoteDBManager.ActivateUser(u.getUsername());
						String info = "User " + login + " successfully logged in! :)";
						JOptionPane.showMessageDialog(this, info);
						System.out.println(info);
						
						OnLoginOrRegisterSuccess(u);
					}
					
				}
				else //wrong pass
				{
					JOptionPane.showMessageDialog(this, "Provided username or password is wrong", "Wrong username or password", JOptionPane.ERROR_MESSAGE);
				}
			}
		}
	}
	
	void OnRegisterAttempt()
	{
		if(loginField.getText().length() < 1) JOptionPane.showMessageDialog(this, "Please enter login", "No login", JOptionPane.ERROR_MESSAGE);
		else if(passwordField.getPassword().length < 6)  JOptionPane.showMessageDialog(this, "Please enter password longer than 6 signs", "No password", JOptionPane.ERROR_MESSAGE);
		else if(passwordFieldConfirm.getPassword().length < 1)  JOptionPane.showMessageDialog(this, "Please confirm your password", "No password confirmation", JOptionPane.ERROR_MESSAGE);
		else if(!(new String(passwordField.getPassword()).equals(new String(passwordFieldConfirm.getPassword()))))  JOptionPane.showMessageDialog(this, "Confirmation password does not match password", "Passwords don't match", JOptionPane.ERROR_MESSAGE);
		else if(emailField.getText().length() < 1 || !emailField.getText().contains("@") || !emailField.getText().contains("."))  JOptionPane.showMessageDialog(this, "Please provide a correct email address", "Wrong email", JOptionPane.ERROR_MESSAGE);
		else
		{
			String login = loginField.getText();
			User u = RemoteDBManager.getUser(login);
			if(u != null) //user exists
			{
				JOptionPane.showMessageDialog(this, "User with provided username already exists. \nUse login form to login if it is you, or choose different username for registration", "Username taken", JOptionPane.ERROR_MESSAGE);
			}
			else
			{
				if(RemoteDBManager.isEmailUsed(emailField.getText())) JOptionPane.showMessageDialog(this, "Provided email address is already registerd.\nPlease login or choose different email for registration", "Email already used", JOptionPane.ERROR_MESSAGE);
				else
				{
					User toAdd = new User(RemoteDBManager.getNewUserId(), login, Hasher.getHashed(new String(passwordField.getPassword())), "", emailField.getText(), Mailer.generateActivationCode());
					if(RemoteDBManager.AddUserToDatabase(toAdd)) //success
					{
						String answer = "";
						
						if(RemoteDBManager.requireEmailConfirmation)
						{
							Thread t = new Thread(new Runnable() {
							    public void run() {
							        
							    	Mailer.SendEmail(RemoteDBManager.config, toAdd.getEmail(), "WK Services App - Activation Code", "Your activation code is: " + toAdd.getActivationCode());
							    }
							});
							t.start();
							answer = JOptionPane.showInputDialog(this, "Please enter activation code sent to your email:");
							while(!answer.contentEquals(toAdd.getActivationCode()) && RemoteDBManager.requireEmailConfirmation)
							{
								answer = JOptionPane.showInputDialog(this, "Wrong activtion code!\nPlease enter activation code sent to your email:");
							}
						}
						
						if(!RemoteDBManager.requireEmailConfirmation || answer.contentEquals(toAdd.getActivationCode()))
						{
							if(RemoteDBManager.requireEmailConfirmation) RemoteDBManager.ActivateUser(toAdd.getUsername());
							String info = "User " + toAdd.getUsername() + " (id " + toAdd.getUser_id() +") was added to user database";
							JOptionPane.showMessageDialog(this, info);
							System.out.println(info);
							
							OnLoginOrRegisterSuccess(toAdd);
						}
					}
					else //fail
					{
						String info = "Failed to add user " + toAdd.getUsername() + " (id " + toAdd.getUser_id() +") to user database";
						JOptionPane.showMessageDialog(this, info, "Registration failed", JOptionPane.WARNING_MESSAGE);
						System.out.println(info);
					}
				}
			}
		}
	}
	
	void OnLoginOrRegisterSuccess(User u)
	{
		System.out.println("On login or register success ....");
		RemoteDBManager.loggedInUser = u;
		

		instance.setVisible(false);
		MainFrame mf = new MainFrame();
		mf.setVisible(true);
	}

}
