import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;

import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

public class DBConfigGenerator extends JFrame 
{
	private JPanel contentPane;
	private JTextField tf_dbname;
	private JTextField tf_dbhost;
	private JTextField tf_jdbcdriver;
	private JTextField tf_dbuser;
	private JPasswordField pf_dbuserpass;
	private JButton loadButton, saveButton;
	private JLabel lblEmailAddress;
	private JLabel lblEmailSenderName;
	private JLabel lblEmailLogin;
	private JLabel lblEmailPassword;
	private JTextField tf_EmailAddress;
	private JTextField tf_EmailSender;
	private JTextField tf_EmailLogin;
	private JPasswordField pf_EmailPass;
	private JLabel lblEmailHost;
	private JLabel lblPort;
	private JTextField tf_EmailPort;
	private JTextField tf_EmailHost;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) 
	{
		
		EventQueue.invokeLater(new Runnable() 
		{
			public void run() {
				try {
					DBConfigGenerator frame = new DBConfigGenerator();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DBConfigGenerator() 
	{
		setTitle("DB Config Manager");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 340);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{0, 0, 0, 0, 0};
		gbl_contentPane.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_contentPane.columnWeights = new double[]{0.0, 1.0, 0.0, 1.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		JLabel label_dbname = new JLabel("Database name");
		GridBagConstraints gbc_label_dbname = new GridBagConstraints();
		gbc_label_dbname.anchor = GridBagConstraints.EAST;
		gbc_label_dbname.insets = new Insets(0, 0, 5, 5);
		gbc_label_dbname.gridx = 0;
		gbc_label_dbname.gridy = 0;
		contentPane.add(label_dbname, gbc_label_dbname);
		
		tf_dbname = new JTextField();
		GridBagConstraints gbc_tf_dbname = new GridBagConstraints();
		gbc_tf_dbname.gridwidth = 3;
		gbc_tf_dbname.insets = new Insets(0, 0, 5, 0);
		gbc_tf_dbname.fill = GridBagConstraints.HORIZONTAL;
		gbc_tf_dbname.gridx = 1;
		gbc_tf_dbname.gridy = 0;
		contentPane.add(tf_dbname, gbc_tf_dbname);
		tf_dbname.setColumns(10);
		
		JLabel label_dbhost = new JLabel("Database host");
		GridBagConstraints gbc_label_dbhost = new GridBagConstraints();
		gbc_label_dbhost.anchor = GridBagConstraints.EAST;
		gbc_label_dbhost.insets = new Insets(0, 0, 5, 5);
		gbc_label_dbhost.gridx = 0;
		gbc_label_dbhost.gridy = 1;
		contentPane.add(label_dbhost, gbc_label_dbhost);
		
		tf_dbhost = new JTextField();
		GridBagConstraints gbc_tf_dbhost = new GridBagConstraints();
		gbc_tf_dbhost.gridwidth = 3;
		gbc_tf_dbhost.insets = new Insets(0, 0, 5, 0);
		gbc_tf_dbhost.fill = GridBagConstraints.HORIZONTAL;
		gbc_tf_dbhost.gridx = 1;
		gbc_tf_dbhost.gridy = 1;
		contentPane.add(tf_dbhost, gbc_tf_dbhost);
		tf_dbhost.setColumns(10);
		
		JLabel label_jdbcdriver = new JLabel("JDBC driver");
		GridBagConstraints gbc_label_jdbcdriver = new GridBagConstraints();
		gbc_label_jdbcdriver.anchor = GridBagConstraints.EAST;
		gbc_label_jdbcdriver.insets = new Insets(0, 0, 5, 5);
		gbc_label_jdbcdriver.gridx = 0;
		gbc_label_jdbcdriver.gridy = 2;
		contentPane.add(label_jdbcdriver, gbc_label_jdbcdriver);
		
		tf_jdbcdriver = new JTextField();
		tf_jdbcdriver.setText("com.mysql.cj.jdbc.Driver");
		GridBagConstraints gbc_tf_jdbcdriver = new GridBagConstraints();
		gbc_tf_jdbcdriver.gridwidth = 3;
		gbc_tf_jdbcdriver.insets = new Insets(0, 0, 5, 0);
		gbc_tf_jdbcdriver.fill = GridBagConstraints.HORIZONTAL;
		gbc_tf_jdbcdriver.gridx = 1;
		gbc_tf_jdbcdriver.gridy = 2;
		contentPane.add(tf_jdbcdriver, gbc_tf_jdbcdriver);
		tf_jdbcdriver.setColumns(10);
		
		JLabel label_dbuser = new JLabel("DB user");
		GridBagConstraints gbc_label_dbuser = new GridBagConstraints();
		gbc_label_dbuser.anchor = GridBagConstraints.BELOW_BASELINE_TRAILING;
		gbc_label_dbuser.insets = new Insets(0, 0, 5, 5);
		gbc_label_dbuser.gridx = 0;
		gbc_label_dbuser.gridy = 3;
		contentPane.add(label_dbuser, gbc_label_dbuser);
		
		tf_dbuser = new JTextField();
		GridBagConstraints gbc_tf_dbuser = new GridBagConstraints();
		gbc_tf_dbuser.gridwidth = 3;
		gbc_tf_dbuser.insets = new Insets(0, 0, 5, 0);
		gbc_tf_dbuser.fill = GridBagConstraints.HORIZONTAL;
		gbc_tf_dbuser.gridx = 1;
		gbc_tf_dbuser.gridy = 3;
		contentPane.add(tf_dbuser, gbc_tf_dbuser);
		tf_dbuser.setColumns(10);
		
		JLabel label_dbuserpass = new JLabel("DB user pass");
		GridBagConstraints gbc_label_dbuserpass = new GridBagConstraints();
		gbc_label_dbuserpass.anchor = GridBagConstraints.EAST;
		gbc_label_dbuserpass.insets = new Insets(0, 0, 5, 5);
		gbc_label_dbuserpass.gridx = 0;
		gbc_label_dbuserpass.gridy = 4;
		contentPane.add(label_dbuserpass, gbc_label_dbuserpass);
		
		pf_dbuserpass = new JPasswordField();
		GridBagConstraints gbc_pf_dbuserpass = new GridBagConstraints();
		gbc_pf_dbuserpass.gridwidth = 3;
		gbc_pf_dbuserpass.insets = new Insets(0, 0, 5, 0);
		gbc_pf_dbuserpass.fill = GridBagConstraints.HORIZONTAL;
		gbc_pf_dbuserpass.gridx = 1;
		gbc_pf_dbuserpass.gridy = 4;
		contentPane.add(pf_dbuserpass, gbc_pf_dbuserpass);
		pf_dbuserpass.setColumns(10);
		
		loadButton = new JButton("Load");
		loadButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) { OnLoadButtonClicked(); }
		});
		
		lblEmailAddress = new JLabel("Email address");
		GridBagConstraints gbc_lblEmailAddress = new GridBagConstraints();
		gbc_lblEmailAddress.anchor = GridBagConstraints.EAST;
		gbc_lblEmailAddress.insets = new Insets(0, 0, 5, 5);
		gbc_lblEmailAddress.gridx = 0;
		gbc_lblEmailAddress.gridy = 5;
		contentPane.add(lblEmailAddress, gbc_lblEmailAddress);
		
		tf_EmailAddress = new JTextField();
		GridBagConstraints gbc_tf_EmailAddress = new GridBagConstraints();
		gbc_tf_EmailAddress.gridwidth = 3;
		gbc_tf_EmailAddress.insets = new Insets(0, 0, 5, 0);
		gbc_tf_EmailAddress.fill = GridBagConstraints.HORIZONTAL;
		gbc_tf_EmailAddress.gridx = 1;
		gbc_tf_EmailAddress.gridy = 5;
		contentPane.add(tf_EmailAddress, gbc_tf_EmailAddress);
		tf_EmailAddress.setColumns(10);
		
		lblEmailSenderName = new JLabel("Email sender name");
		GridBagConstraints gbc_lblEmailSenderName = new GridBagConstraints();
		gbc_lblEmailSenderName.anchor = GridBagConstraints.EAST;
		gbc_lblEmailSenderName.insets = new Insets(0, 0, 5, 5);
		gbc_lblEmailSenderName.gridx = 0;
		gbc_lblEmailSenderName.gridy = 6;
		contentPane.add(lblEmailSenderName, gbc_lblEmailSenderName);
		
		tf_EmailSender = new JTextField();
		GridBagConstraints gbc_tf_EmailSender = new GridBagConstraints();
		gbc_tf_EmailSender.gridwidth = 3;
		gbc_tf_EmailSender.anchor = GridBagConstraints.NORTH;
		gbc_tf_EmailSender.insets = new Insets(0, 0, 5, 0);
		gbc_tf_EmailSender.fill = GridBagConstraints.HORIZONTAL;
		gbc_tf_EmailSender.gridx = 1;
		gbc_tf_EmailSender.gridy = 6;
		contentPane.add(tf_EmailSender, gbc_tf_EmailSender);
		tf_EmailSender.setColumns(10);
		
		lblEmailLogin = new JLabel("Email login");
		GridBagConstraints gbc_lblEmailLogin = new GridBagConstraints();
		gbc_lblEmailLogin.anchor = GridBagConstraints.EAST;
		gbc_lblEmailLogin.insets = new Insets(0, 0, 5, 5);
		gbc_lblEmailLogin.gridx = 0;
		gbc_lblEmailLogin.gridy = 7;
		contentPane.add(lblEmailLogin, gbc_lblEmailLogin);
		
		tf_EmailLogin = new JTextField();
		GridBagConstraints gbc_tf_EmailLogin = new GridBagConstraints();
		gbc_tf_EmailLogin.gridwidth = 3;
		gbc_tf_EmailLogin.insets = new Insets(0, 0, 5, 0);
		gbc_tf_EmailLogin.fill = GridBagConstraints.HORIZONTAL;
		gbc_tf_EmailLogin.gridx = 1;
		gbc_tf_EmailLogin.gridy = 7;
		contentPane.add(tf_EmailLogin, gbc_tf_EmailLogin);
		tf_EmailLogin.setColumns(10);
		
		lblEmailPassword = new JLabel("Email password");
		GridBagConstraints gbc_lblEmailPassword = new GridBagConstraints();
		gbc_lblEmailPassword.anchor = GridBagConstraints.EAST;
		gbc_lblEmailPassword.insets = new Insets(0, 0, 5, 5);
		gbc_lblEmailPassword.gridx = 0;
		gbc_lblEmailPassword.gridy = 8;
		contentPane.add(lblEmailPassword, gbc_lblEmailPassword);
		
		pf_EmailPass = new JPasswordField();
		GridBagConstraints gbc_pf_EmailPass = new GridBagConstraints();
		gbc_pf_EmailPass.gridwidth = 3;
		gbc_pf_EmailPass.insets = new Insets(0, 0, 5, 0);
		gbc_pf_EmailPass.fill = GridBagConstraints.HORIZONTAL;
		gbc_pf_EmailPass.gridx = 1;
		gbc_pf_EmailPass.gridy = 8;
		contentPane.add(pf_EmailPass, gbc_pf_EmailPass);
		
		lblEmailHost = new JLabel("Email host");
		GridBagConstraints gbc_lblEmailHost = new GridBagConstraints();
		gbc_lblEmailHost.anchor = GridBagConstraints.EAST;
		gbc_lblEmailHost.insets = new Insets(0, 0, 5, 5);
		gbc_lblEmailHost.gridx = 0;
		gbc_lblEmailHost.gridy = 9;
		contentPane.add(lblEmailHost, gbc_lblEmailHost);
		
		tf_EmailHost = new JTextField();
		GridBagConstraints gbc_tf_EmailHost = new GridBagConstraints();
		gbc_tf_EmailHost.anchor = GridBagConstraints.NORTH;
		gbc_tf_EmailHost.insets = new Insets(0, 0, 5, 5);
		gbc_tf_EmailHost.fill = GridBagConstraints.HORIZONTAL;
		gbc_tf_EmailHost.gridx = 1;
		gbc_tf_EmailHost.gridy = 9;
		contentPane.add(tf_EmailHost, gbc_tf_EmailHost);
		tf_EmailHost.setColumns(10);
		
		lblPort = new JLabel("Port");
		GridBagConstraints gbc_lblPort = new GridBagConstraints();
		gbc_lblPort.anchor = GridBagConstraints.EAST;
		gbc_lblPort.insets = new Insets(0, 0, 5, 5);
		gbc_lblPort.gridx = 2;
		gbc_lblPort.gridy = 9;
		contentPane.add(lblPort, gbc_lblPort);
		
		tf_EmailPort = new JTextField();
		GridBagConstraints gbc_tf_EmailPort = new GridBagConstraints();
		gbc_tf_EmailPort.insets = new Insets(0, 0, 5, 0);
		gbc_tf_EmailPort.fill = GridBagConstraints.HORIZONTAL;
		gbc_tf_EmailPort.gridx = 3;
		gbc_tf_EmailPort.gridy = 9;
		contentPane.add(tf_EmailPort, gbc_tf_EmailPort);
		tf_EmailPort.setColumns(10);
		GridBagConstraints gbc_loadButton = new GridBagConstraints();
		gbc_loadButton.anchor = GridBagConstraints.SOUTH;
		gbc_loadButton.insets = new Insets(0, 0, 0, 5);
		gbc_loadButton.gridx = 2;
		gbc_loadButton.gridy = 10;
		contentPane.add(loadButton, gbc_loadButton);
		
		saveButton = new JButton("Save");
		saveButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) { OnSaveButtonClicked(); }
		});
		GridBagConstraints gbc_saveButton = new GridBagConstraints();
		gbc_saveButton.anchor = GridBagConstraints.SOUTH;
		gbc_saveButton.gridx = 3;
		gbc_saveButton.gridy = 10;
		contentPane.add(saveButton, gbc_saveButton);
		
	}
	
	void OnLoadButtonClicked()
	{
		//System.out.println("OnLoadButtonClicked");
		JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
		jfc.setDialogTitle("Choose file to load from: ");
		jfc.setFileSelectionMode(JFileChooser.FILES_ONLY);
		jfc.setFileFilter(new FileNameExtensionFilter("JSON files", "json"));

		int returnValue = jfc.showOpenDialog(null);
		if (returnValue == JFileChooser.APPROVE_OPTION ) 
		{
			File filePath = jfc.getSelectedFile();
			if(filePath.exists() && filePath.canRead())
			{
				DBConfig dbc = DBConfig.readFromFile(filePath);
				if(dbc != null)
				{
					tf_dbname.setText(dbc.getDatabase_Name());
					tf_dbhost.setText(dbc.getDatabase_Host());
					tf_jdbcdriver.setText(dbc.getJDBC_Driver());
					tf_dbuser.setText(dbc.getDB_username());
					pf_dbuserpass.setText(dbc.getDB_userpass());
					tf_EmailAddress.setText(dbc.getEmail_senderAddress());
					tf_EmailSender.setText(dbc.getEmail_senderDisplayName());
					tf_EmailLogin.setText(dbc.getEmail_username());
					pf_EmailPass.setText(dbc.getEmail_userpass());
					tf_EmailHost.setText(dbc.getEmail_host());
					tf_EmailPort.setText(dbc.getEmail_port());
				}
			}
		}
	}
	
	void OnSaveButtonClicked()
	{
		//System.out.println("OnSaveButtonClicked");
		JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
		jfc.setDialogTitle("Choose file name and file save location: ");
		jfc.setFileSelectionMode(JFileChooser.FILES_ONLY);
		jfc.setFileFilter(new FileNameExtensionFilter("JSON files", "json"));

		int returnValue = jfc.showSaveDialog(null);
		if (returnValue == JFileChooser.APPROVE_OPTION) 
		{
			File filePath = jfc.getSelectedFile();
			if(!filePath.toString().endsWith(".json")) filePath = new File(filePath.toString() + ".json");
			DBConfig dbc = new DBConfig(tf_dbname.getText(), tf_dbhost.getText(), tf_jdbcdriver.getText(), tf_dbuser.getText(), new String(pf_dbuserpass.getPassword()), tf_EmailAddress.getText(), tf_EmailSender.getText(), tf_EmailLogin.getText(), new String(pf_EmailPass.getPassword()), tf_EmailHost.getText(), tf_EmailPort.getText());
			
			try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filePath), "utf-8"))) 
			{
				writer.write(dbc.getJSON());
				writer.close();
				System.out.println("Config saved to: " + filePath.toString());
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}

}
