import java.util.ArrayList;
import org.json.JSONObject;

public class DailyStockInfoSeries 
{
	private String symbol;
	private ArrayList<SingleDayStockInfo> dailyInfos = new ArrayList<SingleDayStockInfo>();
	
	
	public DailyStockInfoSeries(String symbol, ArrayList<SingleDayStockInfo> dailyInfos) {
		super();
		this.symbol = symbol;
		this.dailyInfos = dailyInfos;
	}
	
	public DailyStockInfoSeries()
	{
		
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public ArrayList<SingleDayStockInfo> getDailyInfos() {
		return dailyInfos;
	}

	public static DailyStockInfoSeries parseFromWeb(String toParseSymbol)
	{
		JSONObject json = DataHandler.readJsonFromUrl(constructParseUrl(toParseSymbol));
		
		JSONObject timeSeriesObject = json.getJSONObject("Time Series (Daily)");
		JSONObject metaDataObject = json.getJSONObject("Meta Data");
		
		String _symbol = metaDataObject.getString("2. Symbol");
		
		if(timeSeriesObject != null && metaDataObject != null)
		{
			DailyStockInfoSeries dsis = new DailyStockInfoSeries();
			dsis.dailyInfos = DailyStockInfoSeries.readInfosFromJsonObject(_symbol, timeSeriesObject);
			System.out.println(dsis.dailyInfos.size());
			
			dsis.parseMetaData(metaDataObject);
			
			return dsis;
		}
		
		return null;
	}
	
	private static String constructParseUrl(String symb)
	{
		String key = APIKeysManager.getInstance().getKey();
		if(key !=null) return "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY_ADJUSTED&symbol=" + symb + "&apikey=" + key;
		else return null;
	}
	
	public void parseNewFromWeb()
	{
		try
		{
			JSONObject json = DataHandler.readJsonFromUrl(constructParseUrl(this.getSymbol()));
			JSONObject timeSeriesObject = json.getJSONObject("Time Series (Daily)");
			JSONObject metaDataObject = json.getJSONObject("Meta Data");
			
			String _symbol = metaDataObject.getString("2. Symbol");
			
			ArrayList<SingleDayStockInfo> _new = DailyStockInfoSeries.readInfosFromJsonObject(_symbol, timeSeriesObject);
			if(_new != null) 
			{
				int ac = this.addNewOnly(_new);
				System.out.println("parseNewFromWeb: " + ac + " info objects added");
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	private boolean parseMetaData(JSONObject metaDataObject)
	{
		try
		{
			String _symbol = metaDataObject.getString("2. Symbol");
			
			if(_symbol != null)
			{
				this.symbol = _symbol;
				
				return true;
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return false;
	}
	
	private int addNewOnly(ArrayList<SingleDayStockInfo> toAddList)
	{
		int counter = 0;
		ArrayList<SingleDayStockInfo> newList = getNew(toAddList);
		for(SingleDayStockInfo sdsi : newList)
			this.dailyInfos.add(sdsi);
		
		return counter;
	}
	
	public int add(ArrayList<SingleDayStockInfo> toAddList)
	{
		int counter = 0;
		for(SingleDayStockInfo sdsi : toAddList)
			this.dailyInfos.add(sdsi);
		
		return counter;
	}
	
	public void add(SingleDayStockInfo toAdd)
	{
		this.dailyInfos.add(toAdd);
	}
	
	public ArrayList<SingleDayStockInfo> getNew(ArrayList<SingleDayStockInfo> other)
	{
		ArrayList<SingleDayStockInfo> newList = new ArrayList<SingleDayStockInfo>();
		for(SingleDayStockInfo sdsi_new : other)
		{
			boolean alreadyOnList = false;
			for(SingleDayStockInfo sdsi_current : dailyInfos)
				if(sdsi_current.equals(sdsi_new)) alreadyOnList = true;
			
			if(!alreadyOnList) newList.add(sdsi_new);
		}
		
		return newList;
	}
	
	private static ArrayList<SingleDayStockInfo> readInfosFromJsonObject(String symbol, JSONObject timeSeriesObject)
	{
		try
		{
			String[] names = JSONObject.getNames(timeSeriesObject);
			
			ArrayList<SingleDayStockInfo> toReturnArray = new ArrayList<SingleDayStockInfo>();
			
			for(String n : names)
			{
				JSONObject thisEntry = timeSeriesObject.getJSONObject(n);
				
				SingleDayStockInfo sdi = new SingleDayStockInfo();
				sdi.setSymbol(symbol);
				sdi.setDate(n);
				sdi.setOpen(thisEntry.getFloat("1. open"));
				sdi.setHigh(thisEntry.getFloat("2. high"));
				sdi.setLow(thisEntry.getFloat("3. low"));
				sdi.setClose(thisEntry.getFloat("4. close"));
				sdi.setAdjusted_close(thisEntry.getFloat("5. adjusted close"));
				sdi.setVolume(thisEntry.getLong("6. volume"));
				sdi.setDividend_amount(thisEntry.getFloat("7. dividend amount"));
				sdi.setSplit_coef(thisEntry.getFloat("8. split coefficient"));
				
				toReturnArray.add(sdi);
			}
			
			return toReturnArray;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	public String toString() {
		return this.symbol;
	}

	public static void main(String[] args) 
	{
		System.out.println("main start");
		
		LocalDBManager.initialize();
		
		DailyStockInfoSeries mydsis = DailyStockInfoSeries.parseFromWeb("SPX");
		
		mydsis.getDailyInfos().get(5).print();
		mydsis.getDailyInfos().get(65).print();
		
		for(SingleDayStockInfo sdsi : mydsis.getDailyInfos())
		{
			LocalDBManager.AddSDSIToDatabase(sdsi);
		}
		
		System.out.println("main finish");
	}
}
