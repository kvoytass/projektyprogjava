import java.lang.reflect.Field;

public class User implements DBEntry
{
	int user_id;
	String username;
	String password;
	String phoneNumber;
	String email;
	String activationCode;
	int activated = -1;
	
	public int getUser_id() {
		return user_id;
	}

	public String getUsername() {
		return username;
	}

	public String getEmail() {
		return email;
	}

	public User(int user_id, String username, String password, String phoneNumber, String email,
			String activationCode) 
	{
		super();
		this.user_id = user_id;
		this.username = username;
		this.password = password;
		this.phoneNumber = phoneNumber;
		this.email = email;
		this.activationCode = activationCode;
		this.activated = -1;
	}

	public String getPassword() {
		return password;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public String getActivationCode() {
		return activationCode;
	}

	public int getActivated() {
		return activated;
	}

	public User(int user_id, String username, String password, String phoneNumber, String email,
			String activationCode, int activated) 
	{
		super();
		this.user_id = user_id;
		this.username = username;
		this.password = password;
		this.phoneNumber = phoneNumber;
		this.email = email;
		this.activationCode = activationCode;
		this.activated = activated;
	}
	
	public String toDatabaseEntryFormat()
	{
		String tr = "";
		Field[] fields = User.class.getDeclaredFields();

		try 
		{
			for(Field f : fields)
			{
				if(f.getType().getSimpleName().toLowerCase().contains("string"))
					tr += "'" + f.get(this).toString() + "', ";
				else tr += f.get(this).toString() + ", ";
			} 
			
			tr = tr.substring(0, tr.length() - 2);
		}
		catch (IllegalArgumentException | IllegalAccessException e) 
		{
			e.printStackTrace();
			return null;
		}
				
		return tr;
	}
}
