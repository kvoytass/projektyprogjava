import java.io.File;
import java.io.FileOutputStream;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class DBConfig
{
	String Database_Name;
	String Database_Host;
	String JDBC_Driver;
	String DB_username;
	String DB_userpass;
    String Email_senderAddress;
    String Email_senderDisplayName;
    String Email_username;
    String Email_userpass;
    String Email_host;
    String Email_port;
	
	public DBConfig(String database_Name, String database_Host, String jDBC_Driver, String dB_username, String dB_userpass, String email_senderAddress, 
			String email_senderDisplayName, String email_username, String email_userpass, String email_host, String email_port) 
	{
		super();
		Database_Name = database_Name;
		Database_Host = database_Host;
		JDBC_Driver = jDBC_Driver;
		DB_username = dB_username;
		DB_userpass = dB_userpass;
	    Email_senderAddress = email_senderAddress;
	    Email_senderDisplayName = email_senderDisplayName;
	    Email_username = email_username;
	    Email_userpass = email_userpass;
	    Email_host = email_host;
	    Email_port = email_port;
	}

	public String getDatabase_Name() 
	{
		return Database_Name;
	}

	public void setDatabase_Name(String database_Name) {
		Database_Name = database_Name;
	}

	public String getDatabase_Host() {
		return Database_Host;
	}

	public void setDatabase_Host(String database_Host) {
		Database_Host = database_Host;
	}

	public String getJDBC_Driver() {
		return JDBC_Driver;
	}

	public void setJDBC_Driver(String jDBC_Driver) {
		JDBC_Driver = jDBC_Driver;
	}

	public String getDB_username() {
		return DB_username;
	}

	public void setDB_username(String dB_username) {
		DB_username = dB_username;
	}

	public String getDB_userpass() {
		return DB_userpass;
	}

	public void setDB_userpass(String dB_userpass) {
		DB_userpass = dB_userpass;
	}

	
	public String getEmail_senderAddress() {
		return Email_senderAddress;
	}

	public String getEmail_senderDisplayName() {
		return Email_senderDisplayName;
	}

	public String getEmail_username() {
		return Email_username;
	}

	public String getEmail_userpass() {
		return Email_userpass;
	}

	public String getEmail_host() {
		return Email_host;
	}

	public String getEmail_port() {
		return Email_port;
	}

	public String getDBUrl() { return "jdbc:mysql://" + Database_Host + "/" + Database_Name + 
			"?useUnicode=true&useJDBCCompliantTimezoneShift=true" + 
			"&useLegacyDatetimeCode=false&serverTimezone=UTC"; }
	
	public String getJSON()
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		
		return gson.toJson(this);
	}
	
	public boolean writeToFile(File file)
	{
        FileOutputStream outputStream;
        try 
        {
            outputStream = new FileOutputStream(file);
            outputStream.write(getJSON().getBytes());
            outputStream.close();
            return true;
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        return false;
	}
	
	public boolean writeToFile(String file)
	{
		File f = new File(file);
		if(f.canWrite()) return writeToFile(f);
		return false;
	}
	
	public static DBConfig readFromFile(File file)
	{
		try 
		{
			java.io.FileInputStream inputStream = new java.io.FileInputStream(file);
			byte[] _read = new byte[1024];
			int l = inputStream.read(_read);
			String s_read = new String(_read);
			s_read = s_read.substring(0, l);
			
			Gson gson = new GsonBuilder().setLenient().create();
			try
			{
				DBConfig c = gson.fromJson(s_read, DBConfig.class);
				
				return c;
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return null;
	}
	
	public static DBConfig readFromFile(String file)
	{
		File f = new File(file);
		if(f.canRead()) return readFromFile(f);
		return null;
	}
}