import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import javax.xml.bind.DatatypeConverter;

public class Hasher 
{
	public static String getHashed(String in)
	{
		byte[] hash;
		
		MessageDigest digest;
		try 
		{
			digest = MessageDigest.getInstance("SHA-256");
			hash = digest.digest(in.getBytes(StandardCharsets.UTF_8));
			String encoded = Base64.getEncoder().encodeToString(hash);
			String out = DatatypeConverter.printHexBinary(hash);
			
			return out;
		} 
		catch (NoSuchAlgorithmException e) 
		{
			e.printStackTrace();
		}
		
		return null;
	}
}
