package project02lab01;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.jdesktop.beansbinding.AutoBinding;
import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.beansbinding.Bindings;
import org.jdesktop.swingbinding.JTableBinding;
import org.jdesktop.swingbinding.SwingBindings;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import net.miginfocom.swing.MigLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class Weather extends JFrame 
{
	class MyHandler extends DefaultHandler 
	{
		Map<String, Boolean> elements;// = new HashMap<String, Boolean>();
		WeatherDataEntry newEntry;
		
		MyHandler(WeatherDataEntry ne)
		{
			elements = new HashMap<String, Boolean>();

			this.newEntry = ne;
		}

		@Override
		public void characters(char[] ch, int start, int length) throws SAXException {
			super.characters(ch, start, length);
			if (elements.get("country")) {
				newEntry.setCountry(new String(ch, start, length));
			}
		}

		@Override
		public void endElement(String uri, String localName, String qName) throws SAXException {
			elements.put(qName, false);
		}

		@Override
		public void startElement(String uri, String localName, String qName, Attributes attributes)
				throws SAXException {
			elements.put(qName, true);
			if (qName.equals("city")) {
				newEntry.setCity(attributes.getValue("name"));
			}
			if (qName.equals("temperature")) {
				newEntry.setTemperature(Float.parseFloat(attributes.getValue("value")));
			}
		}
	}

	private static String weatherXmlUrl = 
			"http://api.openweathermap.org/data/2.5/weather?q=London&mode=xml&apikey=ab3a3a4743c325386addd4cfe19adfba";
	private static final long serialVersionUID = 2835872241589818550L;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					Weather frame = new Weather();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	private JPanel contentPane;

	private WeatherDatabase entries = new WeatherDatabase();
	private JButton btnSaveToDatabase;
	private JTable weatherTable;
	private JTextField textFieldTarget;

	private JTextField textFieldModel;
	private JButton btnLoadDb;
	private JButton btnDeleteDb;

	public Weather() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				AccessWeatherDb.shutdown();
			}
		});
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 500, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new MigLayout("", "[grow]", "[grow][fill][][]"));

		btnSaveToDatabase = new JButton("Check weather");
		btnSaveToDatabase.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				WeatherDataEntry entry = getNewWeatherDataFromUrl();
				entries.addEntry(entry);
				AccessWeatherDb.insertEntry(LocalDateTime.now().hashCode(), entry);
			}
		});

		weatherTable = new JTable();
		contentPane.add(weatherTable, "cell 0 0,grow");
		contentPane.add(btnSaveToDatabase, "flowx,cell 0 1,alignx left,aligny center");

		textFieldTarget = new JTextField();
		contentPane.add(textFieldTarget, "cell 0 2,growx");
		textFieldTarget.setColumns(10);

		textFieldModel = new JTextField();
		contentPane.add(textFieldModel, "cell 0 3,growx");
		textFieldModel.setColumns(10);

		btnLoadDb = new JButton("Load DB");
		btnLoadDb.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				List<WeatherDataEntry> newEntries = null;
				newEntries = AccessWeatherDb.selectWeatherEntries();
				entries.eraseEntries();
				for (WeatherDataEntry e : newEntries) {
					entries.addEntry(e);
				}
			}
		});
		contentPane.add(btnLoadDb, "cell 0 1");

		btnDeleteDb = new JButton("Delete DB");
		btnDeleteDb.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				AccessWeatherDb.deleteAllEntries();
			}
		});
		contentPane.add(btnDeleteDb, "cell 0 1");
		initDataBindings();

		AccessWeatherDb.createConnection();
	}

	private WeatherDataEntry getNewWeatherDataFromUrl() {
		WeatherDataEntry newEntry = new WeatherDataEntry();
		try {
			SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
			parser.parse(weatherXmlUrl, new MyHandler(newEntry));
			newEntry.setTimestamp(new java.sql.Timestamp(System.currentTimeMillis()));
		} catch (Exception e) {
			newEntry.setCity("Error occured");
		}
		return newEntry;
	}

	protected void initDataBindings() {
		BeanProperty<JTextField, String> jTextFieldBeanProperty = BeanProperty.create("text");
		BeanProperty<JTextField, String> jTextFieldBeanProperty_1 = BeanProperty.create("text");
		AutoBinding<JTextField, String, JTextField, String> autoBinding = Bindings.createAutoBinding(
				UpdateStrategy.READ, textFieldModel, jTextFieldBeanProperty, textFieldTarget, jTextFieldBeanProperty_1);
		autoBinding.setConverter(new MyConverter());
		autoBinding.bind();
		//
		BeanProperty<WeatherDatabase, List<WeatherDataEntry>> weatherDatabaseBeanProperty = BeanProperty
				.create("entries");
		JTableBinding<WeatherDataEntry, WeatherDatabase, JTable> jTableBinding = SwingBindings
				.createJTableBinding(UpdateStrategy.READ, entries, weatherDatabaseBeanProperty, weatherTable);
		//
		BeanProperty<WeatherDataEntry, LocalDateTime> weatherDataEntryBeanProperty = BeanProperty.create("timestamp");
		jTableBinding.addColumnBinding(weatherDataEntryBeanProperty).setColumnName("Timestamp");
		//
		BeanProperty<WeatherDataEntry, String> weatherDataEntryBeanProperty_1 = BeanProperty.create("city");
		jTableBinding.addColumnBinding(weatherDataEntryBeanProperty_1).setColumnName("City");
		//
		BeanProperty<WeatherDataEntry, Double> weatherDataEntryBeanProperty_2 = BeanProperty.create("temperature");
		jTableBinding.addColumnBinding(weatherDataEntryBeanProperty_2).setColumnName("Temperature");
		//
		jTableBinding.bind();
	}
}
