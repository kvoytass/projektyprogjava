package project02lab01;

public class MyConverter extends org.jdesktop.beansbinding.Converter<String, String>{

	@Override
	public String convertForward(String arg0) {
		String result = "";
		result = arg0 + "-" + arg0;
		return result;
	}

	@Override
	public String convertReverse(String arg0) {
		String result = "";
		result = arg0.split("-")[0];
		return result;
	}

}