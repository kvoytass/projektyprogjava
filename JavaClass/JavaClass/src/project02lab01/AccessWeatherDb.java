package project02lab01;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;

public class AccessWeatherDb {
	private static String dbURL = "jdbc:derby:C:\\Users\\Andrze Gnatowski\\MyDB;user=test;password=test";
	private static String tableName = "APP.WEATHER";

	private static Connection connection = null;
	private static Statement statement = null;

	public static void createConnection() {
		try {
			connection = DriverManager.getConnection(dbURL);
		} catch (Exception except) {
			except.printStackTrace();
		}
	}

	public static java.util.List<WeatherDataEntry> selectWeatherEntries() {
		java.util.List<WeatherDataEntry> entries = new java.util.LinkedList<WeatherDataEntry>();
		try {
			statement = connection.createStatement();
			ResultSet results = statement.executeQuery("SELECT * FROM " + tableName);

			while (results.next()) {
				String city = results.getString("CITY");
				String country = results.getString("COUNTRY");
				float temperature = results.getFloat("TEMPERATURE");
				java.sql.Timestamp timestamp = results.getTimestamp("TIMESTAMP");
				WeatherDataEntry entry = new WeatherDataEntry(city, country, temperature, timestamp);
				entries.add(entry);
			}
			results.close();
			statement.close();
		} catch (SQLException sqlExcept) {
			sqlExcept.printStackTrace();
		}

		return entries;
	}

	public static void insertEntry(int id, WeatherDataEntry entry) {
		try {
			PreparedStatement preparedStatement = connection
					.prepareStatement("INSERT INTO " + tableName + "(ID, CITY, COUNTRY, TEMPERATURE, TIMESTAMP) values (?, ?, ?, ?, ?)");
			preparedStatement.setInt(1, LocalDateTime.now().hashCode());
			preparedStatement.setString(2, entry.getCity());
			preparedStatement.setString(3, entry.getCountry());
			preparedStatement.setFloat(4, (float) entry.getTemperature());
			preparedStatement.setTimestamp(5, entry.getTimestamp());
			preparedStatement.executeUpdate();
		} catch (SQLException sqlExcept) {
			sqlExcept.printStackTrace();
		}
	}
	
	public static void deleteAllEntries() {
		try {
			statement = connection.createStatement();
			statement.executeUpdate("DELETE FROM " + tableName);
		} catch (SQLException sqlExcept) {
			sqlExcept.printStackTrace();
		}
	}

	public static void shutdown() {
		try {
			if (statement != null) {
				statement.close();
			}
			if (connection != null) {
				DriverManager.getConnection(dbURL + ";shutdown=true");
				connection.close();
			}
		} catch (SQLException sqlExcept) {

		}
	}
}
