package project02lab01;

public class WeatherDataEntry extends AbstractModelObject{
	private String city;
	private String country;
	private float temperature;
	private java.sql.Timestamp timestamp;
	public WeatherDataEntry() {
		super();
		city = "undefined";
	}
	/**
	 * @param city
	 * @param country
	 * @param temperature
	 * @param timestamp
	 */
	public WeatherDataEntry(String city, String country, float temperature, java.sql.Timestamp timestamp) {
		super();
		this.city = city;
		this.country = country;
		this.temperature = temperature;
		this.timestamp = timestamp;
	}
	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}
	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}
	/**
	 * @return the temperature
	 */
	public double getTemperature() {
		return temperature;
	}
	/**
	 * @return the timestamp
	 */
	public java.sql.Timestamp getTimestamp() {
		return timestamp;
	}
	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		String oldValue = this.city;
		firePropertyChange("city", oldValue, city);
		this.city = city;
	}
	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		String oldValue = this.country;
		firePropertyChange("country", oldValue, country);
		this.country = country;
	}
	/**
	 * @param temperature the temperature to set
	 */
	public void setTemperature(float temperature) {
		double oldValue = this.temperature;
		firePropertyChange("temperature", oldValue, temperature);
		this.temperature = temperature;
	}
	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(java.sql.Timestamp timestamp) {
		java.sql.Timestamp oldValue = this.timestamp;
		firePropertyChange("timestamp", oldValue, timestamp);
		this.timestamp = timestamp;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "WeatherData [city=" + city + ", country=" + country + ", temperature=" + temperature + ", timestamp="
				+ timestamp + "]";
	} 
}
