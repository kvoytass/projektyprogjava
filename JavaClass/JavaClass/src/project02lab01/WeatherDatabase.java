package project02lab01;

import java.util.ArrayList;
import java.util.List;

public class WeatherDatabase extends AbstractModelObject {
	private List<WeatherDataEntry> entries = new ArrayList<WeatherDataEntry>();
	
	WeatherDatabase(){	
	}
	
	public void addEntry(WeatherDataEntry entry) {
		List<WeatherDataEntry> oldValue = entries;
		entries = new ArrayList<WeatherDataEntry>(entries);
		entries.add(entry);
		firePropertyChange("entries", oldValue, entries);
		firePropertyChange("entriesCount", oldValue.size(), entries.size());
	}
	
	public List<WeatherDataEntry> getEntries() {
		return entries;
	}
	
	public void eraseEntries() {
		List<WeatherDataEntry> oldValue = entries;
		entries = new ArrayList<WeatherDataEntry>();
		firePropertyChange("entries", oldValue, entries);
		firePropertyChange("entriesCount", oldValue.size(), entries.size());
	}
	
	public int getEntriesCount() {
		return entries.size();
	}
}
