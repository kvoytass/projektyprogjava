import java.awt.Color;

public class MovingWall extends Wall
{
	int moveLen;
	boolean moveInDir = true;
	int progress = 0;
	Direction direction;

	public MovingWall(int startX, int startY, Direction buildDir, int length, Color color, int moveLen, Direction moveDirection)
	{
		super(startX, startY, buildDir, length, color);
		this.moveLen = moveLen;
		direction = moveDirection;
	}
	
	public void Move()
	{
		for(Fragment frag : body)
		{
			if(moveInDir)
				frag.Move(direction);
			else frag.MoveO(direction);
		}
		
		if(moveInDir) progress++;
		else progress--;
		
		if(progress == moveLen - 1) moveInDir = false;
		if(progress == 0) moveInDir = true;
	}

}
