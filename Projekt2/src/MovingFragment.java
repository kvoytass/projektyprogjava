import java.awt.Color;

public class MovingFragment extends Fragment
{
	Direction direction;

	public MovingFragment(int x, int y, Color color, Direction direction) {
		super(x, y, color);
		this.direction = direction;
	}
	
	public MovingFragment(MovingFragment toCopy)
	{
		super(toCopy.x, toCopy.y, toCopy.color);
		this.direction = toCopy.direction;
	}

	public Direction getDirection() {
		return direction;
	}

	public void setDirection(Direction direction) {
		this.direction = direction;
	}
}
