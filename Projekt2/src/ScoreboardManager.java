import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ScoreboardManager
{
	static ScoreboardManager instance;
	public static ScoreboardManager getInstance()
	{
		if (instance == null)
		{
			instance = new ScoreboardManager();
		}
		
		return instance;
	}
	
	class Score
	{
		String name;
		Date date;
		int length;
		
		
		public Score(String name, Date date, int length)
		{
			super();
			this.name = name;
			this.date = date;
			this.length = length;
		}
	}
	
	class ScoreComparator implements Comparator<Score>
	{

		@Override
		public int compare(Score o1, Score o2)
		{
			return -Integer.compare(o1.length, o2.length);
		}
		
	}
	
	public class Scoreboard
	{
		ArrayList<Score> scoresList;
		
		public Scoreboard()
		{
			scoresList = new ArrayList<Score>();
		}
		
		public void add(Score toAdd)
		{
			scoresList.add(toAdd);
			Collections.sort(scoresList, new ScoreComparator());
			while(scoresList.size() > 10) scoresList.remove(10);
		}
		
		public  ArrayList<Score> getScoresList() { return scoresList; }
	}
	
	private Scoreboard scoreboard;
	private final String path = System.getProperty("user.dir") + File.separator + "scores.json";
	private static GsonBuilder builder;
	private static Gson gson;
	
	ScoreboardManager()
	{
		if(builder == null)
		{
			builder = new GsonBuilder(); 
			builder.setPrettyPrinting(); 
		}
		if(gson == null) gson = builder.create();
		
		File file = new File(path);
		if(file.exists())
		{
			String readContent;
			try
			{
				readContent = new String(Files.readAllBytes(Paths.get(path)), StandardCharsets.UTF_8);
				scoreboard = gson.fromJson(readContent, Scoreboard.class);
//				if(scoreboard != null)
//				{
//					System.out.println("Read scoreboard from file:");
//					System.out.println(gson.toJson(scoreboard));
//				}
			} catch (IOException e)
			{
				e.printStackTrace();
				scoreboard = null;
			}
		}
		
		if(scoreboard == null) scoreboard = new Scoreboard();
	}
	
	public void addScore(String name, int length)
	{
		scoreboard.add(new Score(name, new Date(), length));
		saveToFile();
	}
	
	public String getScores()
	{
		if(scoreboard.getScoresList().size() == 0) return "No scores saved";
		
		String scoresList = "No.\tName\tLength\tDate\n";
		
		ArrayList<Score> scores = scoreboard.getScoresList();
		for(int i = 1; i <= scores.size(); i++)
		{
			scoresList += i + ".\t" + scores.get(i-1).name + "\t" + scores.get(i-1).length + "\t" + scores.get(i-1).date +"\n";
		}
		
		return scoresList;
	}
	
	void saveToFile()
	{
		if(scoreboard != null)
		{
//			scoreboard.add(new Score("Wojtek", new Date(), 90));
//			scoreboard.add(new Score("Mati", new Date(), 60));

			String jsonString = gson.toJson(scoreboard); 
//			System.out.println(jsonString);
			
			try
			{
				Files.write(Paths.get(path), jsonString.getBytes(), StandardOpenOption.CREATE);
				//System.out.println("saved to " + path);
			} catch (IOException e)
			{
				e.printStackTrace();
			}
		}
	}
	
	public static void main(String[] args)
	{
		ScoreboardManager.getInstance();
	}
}
