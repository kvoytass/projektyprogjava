import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JOptionPane;

public class GameController
{
	public static GameController instance;
	GameWindow gw;
	static RankingDialog rd;
	SceneMatrix sm;
	Timer timer;
	long period = 250;
	
	private final int cols = 22;
	private final int rows = 22;
	
	boolean isRunning = false;
	ArrayList<SceneObject> snakes;
	ArrayList<SceneObject> walls;
	ArrayList<SceneObject> fruits;
	ArrayList<SceneObject> movingWalls;
	
	class Repeat extends TimerTask
	{
		@Override
		public void run()
		{
			OnScheduledCall();
		}
	}
	
	public GameController()
	{
		instance = this;
		sm = new SceneMatrix(Color.WHITE, cols, rows);
		gw = new GameWindow("Snake Pro v. 999999", cols, rows);
		gw.setVisible(true);
		
		if(rd == null) rd = new RankingDialog();
		
		timer = new Timer();
		TimerTask task = new Repeat();
		timer.schedule(task, period, period);
		
		gw.SetPauseButtonEnabled(false);
		gw.SetStopButtonEnabled(false);
	}
	
	void initializeGame()
	{ 
		snakes = new ArrayList<SceneObject>();
		snakes.add(new Snake("WojtekSnake", 8, 8, Direction.Up, Color.ORANGE, KeyEvent.VK_UP, KeyEvent.VK_RIGHT, KeyEvent.VK_DOWN, KeyEvent.VK_LEFT));
		//snakes.add(new Snake("AterSnejk", 16, 16, Direction.Left, Color.CYAN, KeyEvent.VK_W, KeyEvent.VK_D, KeyEvent.VK_S, KeyEvent.VK_A));
		walls = new ArrayList<SceneObject>();
		walls.add(Wall.GenerateBoundaryWall(cols, rows, Color.BLACK));
		walls.add(new Wall(3, 3, Direction.Right, 5, Color.DARK_GRAY));
		fruits = new ArrayList<SceneObject>();
		fruits.add(new Apple(14, 11));
		fruits.add(new Strawberry(10, 10, 8));
		fruits.add(new Grape(1, 18, walls));
		movingWalls = new ArrayList<SceneObject>();
		movingWalls.add(new MovingWall(4, 17, Direction.Right, 3, Color.LIGHT_GRAY, 4, Direction.Right));
	}
	
	public int getCols()
	{
		return cols;
	}

	public int getRows()
	{
		return rows;
	}
	
	public void onKeyPressed(KeyEvent keyEvent)
	{
		//System.out.println("Key pressed");
		for(SceneObject s : snakes)
		{
			((Snake)s).setDirection(keyEvent.getKeyCode());
		}
	}
	
	public void OnStartButtonClick()
	{
		System.out.println("OnStartButtonClick");
		
		if(!isRunning)
		{
			initializeGame();
			isRunning = true;
			gw.Redraw(sm.Update(snakes, walls, fruits, movingWalls));
			gw.SetStartButtonEnabled(false);
			gw.SetPauseButtonEnabled(true);
			gw.SetStopButtonEnabled(true);
		}
	}
	
	public void OnPauseButtonClick()
	{
		gw.SwitchPauseButtonToResume(true);
		isRunning = false;
		System.out.println("pause");
	}
	
	public void OnResumeButtonClick()
	{
		gw.SwitchPauseButtonToResume(false);
		isRunning = true;
		System.out.println("resume");
	}
	
	public void OnRankingButtonClick()
	{
		showRanking();
	}
	
	public void OnStopButtonClick()
	{
		isRunning = false;
		gw.SetPauseButtonEnabled(false);
		gw.SetStopButtonEnabled(false);
		gw.SetStartButtonEnabled(true);
	}
	
	public void OnScheduledCall()
	{
		
		if(isRunning)
		{
			moveSnakes();
			
			if(isRunning)
			{
				checkIfFruitsWereEaten();
				checkIfFruitsNeedRegeneration();
				moveWalls();
				if(!checkMovingWallCollisionsWithSnakes())
					gw.Redraw(sm.Update(snakes, walls, fruits, movingWalls));
			}

		}
	}
	
	private boolean checkMovingWallCollisionsWithSnakes()
	{
		for(SceneObject s : snakes)
		{
			Snake snk = (Snake)s;
			if(snk.isAlive())
			{
				if(snk.CheckMovingWallsCollisions(movingWalls))
				{
					endGame(snk);
					return true;
				}
			}
		}
		
		return false;
	}

	void moveWalls()
	{
		for(SceneObject s : movingWalls)
		{
			MovingWall mv = (MovingWall)s;
			mv.Move();
		}
	}
	
	void moveSnakes()
	{
		for(SceneObject s : snakes)
		{
			Snake snk = (Snake)s;
			if(snk.isAlive())
			{
				if(!snk.TryMove(snakes, walls, movingWalls))
				{
					System.out.println("Snake " + snk.getName() + " has died :(");
					
					if(checkIfGameEnd()) endGame(snk);
				}
			}
		}
	}
	
	void showRanking()
	{
		rd.setText(ScoreboardManager.getInstance().getScores());
		rd.setVisible(true);
		//JOptionPane.showMessageDialog(gw, ScoreboardManager.getInstance().getScores());
	}
	
	void endGame(Snake winner)
	{
		isRunning = false;
		gw.SetStartButtonEnabled(true);
		gw.SetPauseButtonEnabled(false);
		gw.SetStopButtonEnabled(false);
		gw.SetStartButtonEnabled(true);
		
		int answer = JOptionPane.showConfirmDialog(
			    gw,
			    "Last survivor snake: " + winner.getName() + "\nLength: " + winner.getBody().size() + "\nSave score?", "Game finished!",
			    JOptionPane.YES_NO_OPTION);
		if (answer == 0) //save and show ranking
		{
			ScoreboardManager.getInstance().addScore(winner.getName(), winner.getBody().size());
			showRanking();
		}
	}
	
	boolean checkIfGameEnd()
	{
		for(SceneObject so : snakes)
		{
			if(((Snake)so).isAlive()) return false;
		}
		
		return true;
	}
	
	void checkIfFruitsWereEaten()
	{
		for(SceneObject so : fruits)
		{
			Fruit f = (Fruit)so;
			if(f.checkIfWasEaten(snakes))
			{
				f.regenerate(snakes, walls, fruits);
			}
		}
	}
	
	void checkIfFruitsNeedRegeneration()
	{
		for(SceneObject so : fruits)
		{
			Fruit f = (Fruit)so;
			if(f.needsRegeneration())
			{
				f.regenerate(snakes, walls, fruits);
			}
		}
	}
	
	void Reschedule(long newPeriod)
	{
		period = newPeriod;
		timer.cancel();
		timer.purge();
		timer = new Timer();
		TimerTask task = new Repeat();
		
        timer.schedule(task, period, period); 
	}
	
	public static void main(String[] args)
	{
		new GameController();
	}
}
