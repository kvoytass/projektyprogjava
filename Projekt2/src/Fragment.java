import java.awt.Color;
import java.util.ArrayList;

public class Fragment 
{
	int x, y;
	Color color;
	
	public Fragment(int x, int y, Color color) 
	{
		super();
		this.x = x;
		this.y = y;
		this.color = color;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}
	
	public void Move(Direction dir)
	{
		x += dir.delta_x;
		y += dir.delta_y;
	}
	
	public void MoveO(Direction dir)
	{
		x -= dir.delta_x;
		y -= dir.delta_y;
	}
	
	public Fragment getCollision(ArrayList<Fragment> listToCheck)
	{
		for(Fragment fg : listToCheck)
		{
			if(fg.x == this.x && fg.y == this.y) return fg;
		}
		return null;
	}
	
	public Fragment getCollision(ArrayList<Fragment> listToCheck, Fragment exception)
	{
		for(Fragment fg : listToCheck)
		{
			if(fg != exception && fg.x == this.x && fg.y == this.y) return fg;
		}
		return null;
	}
	
}
