import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.JButton;
import java.awt.Insets;
import java.awt.Toolkit;

import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class GameWindow extends JFrame
{
	class TableColorRenderer extends DefaultTableCellRenderer
	{
		SceneMatrix sm;
		
		public void Update(SceneMatrix newSM)
		{
			sm = newSM;
		}
		
	    @Override
	    public Component getTableCellRendererComponent(
	        JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
	    {
	        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

	        if(sm == null) setBackground(Color.WHITE);
	        else setBackground(sm.getColorMatrix()[column][GameController.instance.getRows() - 1 - row]);

	        return this;
	    }
	}
	
	class SpecialTableModel extends DefaultTableModel 
	{
		public SpecialTableModel(Object[][] objects, String[] strings) 
		{
			this.setDataVector(objects, strings);
		}

		@Override
		public boolean isCellEditable(int row, int column)
		{  
			return false;  
		}

	}

	private JPanel contentPane;
	private JTable table;
	private TableColorRenderer TCR;
	private JButton startButton, pauseButton, rankingButton, stopButton;

	public GameWindow(String windowTitle, int cols, int rows)
	{
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, Math.max(20 * cols + 30, 335), 20 * rows + 83);
		contentPane = new JPanel();
		contentPane.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) 
			{
				GameController.instance.onKeyPressed(e);
			}
		});
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{0, 0, 0, 0, 0};
		gbl_contentPane.rowHeights = new int[]{0, 0, 0};
		gbl_contentPane.columnWeights = new double[]{1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		table = new JTable();
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				contentPane.requestFocus();
			}
		});
		table.setModel(new SpecialTableModel(
			new Object[cols][rows],
			new String[cols]
		));
		
		for(int i = 0; i < cols; i++)
		{
			table.getColumnModel().getColumn(i).setPreferredWidth(20);
			table.getColumnModel().getColumn(i).setMinWidth(20);
			table.getColumnModel().getColumn(i).setMaxWidth(20);
		}
		table.setRowHeight(20);
		
		TCR = new TableColorRenderer();
		table.setDefaultRenderer(Object.class, TCR);
		
		GridBagConstraints gbc_table = new GridBagConstraints();
		gbc_table.gridwidth = 4;
		gbc_table.insets = new Insets(0, 0, 5, 5);
		gbc_table.fill = GridBagConstraints.BOTH;
		gbc_table.gridx = 0;
		gbc_table.gridy = 0;
		contentPane.add(table, gbc_table);
		
		startButton = new JButton("START");
		startButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				contentPane.requestFocus();
				GameController.instance.OnStartButtonClick();
			}
		});

		GridBagConstraints gbc_startButton = new GridBagConstraints();
		gbc_startButton.insets = new Insets(0, 0, 0, 5);
		gbc_startButton.gridx = 0;
		gbc_startButton.gridy = 1;
		contentPane.add(startButton, gbc_startButton);
		
		pauseButton = new JButton("PAUSE");
		pauseButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) 
			{
				contentPane.requestFocus();
				
				if(pauseButton.getText() == "PAUSE")
					GameController.instance.OnPauseButtonClick();
				else
					GameController.instance.OnResumeButtonClick();
			}
		});
		GridBagConstraints gbc_pauseButton = new GridBagConstraints();
		gbc_pauseButton.insets = new Insets(0, 0, 0, 5);
		gbc_pauseButton.gridx = 1;
		gbc_pauseButton.gridy = 1;
		contentPane.add(pauseButton, gbc_pauseButton);
		
		rankingButton = new JButton("Ranking");
		rankingButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				contentPane.requestFocus();
				GameController.instance.OnRankingButtonClick();
			}
		});
		GridBagConstraints gbc_rankingButton = new GridBagConstraints();
		gbc_rankingButton.insets = new Insets(0, 0, 0, 5);
		gbc_rankingButton.gridx = 2;
		gbc_rankingButton.gridy = 1;
		contentPane.add(rankingButton, gbc_rankingButton);
		
		stopButton = new JButton("STOP");
		stopButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				contentPane.requestFocus();
				GameController.instance.OnStopButtonClick();
			}
		});
		GridBagConstraints gbc_stopButton = new GridBagConstraints();
		gbc_stopButton.gridx = 3;
		gbc_stopButton.gridy = 1;
		contentPane.add(stopButton, gbc_stopButton);
	
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
		setTitle(windowTitle);
		contentPane.requestFocus();
	}
	
	public void Redraw(SceneMatrix sm)
	{
		TCR.Update(sm);
		table.repaint();
	}
	
	public void SetStartButtonEnabled(boolean b)
	{
		startButton.setEnabled(b);
	}
	
	public void SetPauseButtonEnabled(boolean b)
	{
		pauseButton.setEnabled(b);
	}
	
	public void SetRankingButtonEnabled(boolean b)
	{
		rankingButton.setEnabled(b);
	}
	
	public void SetStopButtonEnabled(boolean b)
	{
		stopButton.setEnabled(b);
	}
	
	public void SwitchPauseButtonToResume(boolean b)
	{
		if(b) pauseButton.setText("RESUME");
		else pauseButton.setText("PAUSE");
	}

}
