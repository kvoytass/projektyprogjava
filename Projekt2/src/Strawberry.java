import java.awt.Color;

public class Strawberry extends Fruit
{
	int needsRegenerationAfter = 5;
	int roundsGrowing = 0;

	public Strawberry(int x, int y, int nRA)
	{
		super(x, y, Color.RED);
		needsRegenerationAfter = nRA;
		roundsGrowing = 0;
	}

	@Override
	void setNewPos()
	{
		this.body.get(0).setX(random.nextInt(GameController.instance.getCols()));
		this.body.get(0).setY(random.nextInt(GameController.instance.getRows()));
		roundsGrowing = 0;
	}

	@Override
	public boolean needsRegeneration()
	{
		if(++roundsGrowing > needsRegenerationAfter) return true;
		return false;
	}

}
