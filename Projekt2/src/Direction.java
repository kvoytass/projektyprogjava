
public class Direction 
{
	int delta_x, delta_y;

	public Direction(int delta_x, int delta_y) {
		super();
		this.delta_x = delta_x;
		this.delta_y = delta_y;
	}

	public int getDelta_x() {
		return delta_x;
	}

	public void setDelta_x(int delta_x) {
		this.delta_x = delta_x;
	}

	public int getDelta_y() {
		return delta_y;
	}

	public void setDelta_y(int delta_y) {
		this.delta_y = delta_y;
	}
	
	public static final Direction Right = new Direction(1, 0);
	
	public static final Direction Left = new Direction(-1, 0);
	
	public static final Direction Up = new Direction(0, 1);
	
	public static final Direction Down = new Direction(0, -1);
	
	public boolean isRight()
	{
		if(delta_x == 1 && delta_y == 0) return true;
		return false;
	}
	
	public boolean isLeft()
	{
		if(delta_x == -1 && delta_y == 0) return true;
		return false;
	}
	
	public boolean isUp()
	{
		if(delta_x == 0 && delta_y == 1) return true;
		return false;
	}
	
	public boolean isDown()
	{
		if(delta_x == 0 && delta_y == -1) return true;
		return false;
	}

}
