import java.awt.Color;
import java.util.ArrayList;
import java.util.Random;

public class Grape extends Fruit
{
	ArrayList<SceneObject> wallsList;

	public Grape(int x, int y, ArrayList<SceneObject> wallsList)
	{
		super(x, y, Color.MAGENTA);
		this.wallsList = wallsList;
	}

	@Override
	public boolean needsRegeneration()
	{
		return false;
	}

	@Override
	void setNewPos()
	{	
		ArrayList<Fragment> allWallFragments = new ArrayList<Fragment>();
		int fragsCount = 0;
		for(SceneObject so : wallsList)
		{
			for(Fragment frag : so.getBody())
			{
				allWallFragments.add(frag);
				fragsCount++;
			}
		}
		
		Fragment frag = allWallFragments.get(random.nextInt(fragsCount));
		
		int dir = random.nextInt(4);
		if(dir == 0)
		{
			body.get(0).setX(frag.getX() + 1);
			body.get(0).setY(frag.getY());
		}
		else if(dir == 1)
		{
			body.get(0).setX(frag.getX() - 1);
			body.get(0).setY(frag.getY());
		}
		else if(dir == 2)
		{
			body.get(0).setX(frag.getX());
			body.get(0).setY(frag.getY() + 1);
		}
		else
		{
			body.get(0).setX(frag.getX());
			body.get(0).setY(frag.getY() - 1);
		}
	}

}
