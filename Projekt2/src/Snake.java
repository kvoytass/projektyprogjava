import java.awt.Color;
import java.util.ArrayList;

public class Snake extends SceneObject
{
	static final boolean removeOnDeath = false;
	
	String name;
	boolean alive = true;
	int keyUp, keyRight, keyDown, keyLeft;
	MovingFragment previousLast;

	public Snake(String name, int startX, int startY, Direction initialDirection, Color snakeColor, int keyUp, int keyRight, int keyDown, int keyLeft)
	{
		super(snakeColor);
		this.name = name;
		this.keyUp = keyUp;
		this.keyRight = keyRight;
		this.keyDown = keyDown;
		this.keyLeft = keyLeft;
		
		body.add(new MovingFragment(startX, startY, snakeColor, initialDirection));
	}
	
	public boolean isAlive()
	{
		return alive;
	}
	
	public String getName()
	{
		return name;
	}
	
	public Direction getDirection()
	{
		if(body != null && body.size() > 0) return ((MovingFragment)body.get(0)).getDirection();
		
		return null;
	}
	
	public void setDirection(int keyPressed)
	{
		if(keyPressed == keyUp)
		{
			if(body != null && body.size() == 1) ((MovingFragment)body.get(0)).setDirection(Direction.Up);
			else if (body != null)
			{
				if(!(body.get(1).getX() - body.get(0).getX() == 0 && body.get(1).getY() - body.get(0).getY() == 1))
					((MovingFragment)body.get(0)).setDirection(Direction.Up);
			}
		}
		else if(keyPressed == keyRight)
		{
			if(body != null && body.size() == 1) ((MovingFragment)body.get(0)).setDirection(Direction.Right);
			else if (body != null)
			{
				if(!(body.get(1).getX() - body.get(0).getX() == 1 && body.get(1).getY() - body.get(0).getY() == 0))
					((MovingFragment)body.get(0)).setDirection(Direction.Right);
			}
		}
		else if(keyPressed == keyDown)
		{
			if(body != null && body.size() == 1) ((MovingFragment)body.get(0)).setDirection(Direction.Down);
			else if (body != null)
			{
				if(!(body.get(1).getX() - body.get(0).getX() == 0 && body.get(1).getY() - body.get(0).getY() == -1))
					((MovingFragment)body.get(0)).setDirection(Direction.Down);
			}
		}
		else if(keyPressed == keyLeft)
		{
			if(body != null && body.size() == 1) ((MovingFragment)body.get(0)).setDirection(Direction.Left);
			else if (body != null)
			{
				if(!(body.get(1).getX() - body.get(0).getX() == -1 && body.get(1).getY() - body.get(0).getY() == 0))
					((MovingFragment)body.get(0)).setDirection(Direction.Left);
			}
		}
	}
	
	public boolean TryMove(ArrayList<SceneObject> ... collidingObjectsLists)
	{
		if(alive)
		{
			
			MovingFragment newHead = new MovingFragment((MovingFragment)body.get(0));
			newHead.Move(newHead.direction);
			
			previousLast = (MovingFragment)body.get(body.size() - 1); // last
			body.remove(previousLast);
			 
			body.add(0, newHead);
			
			if(newHead.getCollision(body, newHead) != null )
			{
				alive = false;
				body.remove(0);
				body.add(previousLast);
				System.out.println("OWN COLLISION");
				return false;
			}
			
			for(ArrayList<SceneObject> collidingObjectsList : collidingObjectsLists)
			{
				for(SceneObject so : collidingObjectsList)
				{
					if(this != so)
					{
						for(Fragment frag : so.getBody())
						{
							if(frag.getCollision(body) != null)
							{
								alive = false;
								body.remove(0);
								body.add(previousLast);
								System.out.println("OTHER COLLISION");
								return false;
							}
						}
					}
				}
			}
			
			return true;
		}
		
		return false;
	}
	
	public boolean CheckMovingWallsCollisions(ArrayList<SceneObject> movingWallsList)
	{
		if(!alive) return false;

		for(SceneObject so : movingWallsList)
		{
			if(this != so)
			{
				for(Fragment frag : so.getBody())
				{
					if(frag.getCollision(body) != null)
					{
						alive = false;
						body.remove(0);
						body.add(previousLast);
						System.out.println("MOVING WALL COLIDED INTO SNAKE");
						return true;
					}
				}
			}
		}
			
		
		return false;
	}
	
	public void grow()
	{
		if(previousLast != null)
		{
			body.add(previousLast);
			previousLast = null;
		}
	}
	
}
