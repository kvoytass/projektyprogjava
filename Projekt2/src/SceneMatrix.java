import java.awt.Color;
import java.util.ArrayList;

public class SceneMatrix
{
	int rows, cols;
	Color[][] colorMatrix;
	Color backgroundColor;
	
	public Color[][] getColorMatrix()
	{
		return colorMatrix;
	}
	
	public SceneMatrix(Color backgroundColor, int cols, int rows)
	{
		colorMatrix = new Color[cols][rows];
		this.rows = rows; 
		this.cols = cols;
		Reset();
	}
	
	public SceneMatrix Update(ArrayList<SceneObject> ... objectsLists)
	{
		Reset();
		
		for(ArrayList<SceneObject> objectsList : objectsLists)
		{
			for(SceneObject so : objectsList)
			{
				for(Fragment frag : so.getBody())
				{
					if(frag != null && frag.getX() >= 0 && frag.getX() < cols && frag.getY() >= 0 && frag.getY() < rows)
					{
						colorMatrix[frag.getX()][frag.getY()] = frag.getColor();
					}
				}
			}
		}
		
		return this;
	}
	
	void Reset()
	{
		for(int r = 0; r < rows; r++)
			for(int c = 0; c < cols; c++)
				colorMatrix[r][c] = backgroundColor;
	}
}
