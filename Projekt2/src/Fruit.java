import java.awt.Color;
import java.util.ArrayList;
import java.util.Random;

public abstract class Fruit extends SceneObject
{
	static Random random;
	
	public Fruit(int x, int y, Color color)
	{
		super(color);
		if(random == null) random = new Random();
		body.add(new Fragment(x, y, color));
	}

	public boolean checkIfWasEaten(ArrayList<SceneObject> snakes)
	{
		for(SceneObject so : snakes)
		{
			Snake snk = (Snake)so;
			if(snk.getBody().get(0).getX() == this.body.get(0).getX() && snk.getBody().get(0).getY() == this.body.get(0).getY())
			{
				snk.grow();
				return true;
			}
		}
		
		return false;
	}
	
	public void regenerate(ArrayList<SceneObject> ... allObjectLists)
	{
		boolean colliding = true;
	
		setNewPos();
	
		int loopCt = 0;
		
		while(colliding)
		{	
			loopCt++;
			colliding = false;
			
			if(body.get(0).getX() >= GameController.instance.getCols() || body.get(0).getX() < 0
				||body.get(0).getY() >= GameController.instance.getRows() || body.get(0).getY() < 0)
			{
				colliding = true;
				setNewPos();
				continue;
			}
			
			for(ArrayList<SceneObject> objectsList : allObjectLists)
			{
				if(colliding) continue;
				for(SceneObject so : objectsList)
				{
					if(body.get(0).getCollision(so.getBody(), body.get(0)) != null)
					{
						colliding = true;
						setNewPos();
						continue;
					}
				}
			}
		}
	}
	
	public abstract boolean needsRegeneration();
	
	abstract void setNewPos();
}
