import java.awt.Color;
import java.util.ArrayList;

public class Apple extends Fruit
{

	public Apple(int x, int y)
	{
		super(x, y, Color.GREEN);
	}

	@Override
	void setNewPos()
	{
		this.body.get(0).setX(random.nextInt(GameController.instance.getCols()));
		this.body.get(0).setY(random.nextInt(GameController.instance.getRows()));
	}

	@Override
	public boolean needsRegeneration()
	{
		return false;
	}


}
