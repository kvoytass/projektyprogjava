import java.awt.Color;
import java.util.ArrayList;

public class Wall extends SceneObject
{
	private Wall(Color color)
	{
		super(color);
	}
	
	public Wall (int startX, int startY, Direction dir, int length, Color color)
	{
		super(color);
		
		for(int i = 0; i < length; i++)
		{
			body.add(new Fragment(startX + i * dir.getDelta_x(), startY + i * dir.getDelta_y(), color));
		}
	}
	
	public static Wall GenerateBoundaryWall(int cols, int rows, Color color)
	{
		Wall wall = new Wall(color);
		
		for(int c = 0; c < cols; c++)
		{
			wall.body.add(new Fragment(c, 0, wall.color)); // lower boundary
			wall.body.add(new Fragment(c, rows-1, wall.color)); // upper boundary
		}
		
		for(int r = 1; r < rows - 1; r++)
		{
			wall.body.add(new Fragment(0, r, wall.color)); // left boundary
			wall.body.add(new Fragment(cols - 1, r, wall.color)); // right boundary
		}
		
		return wall;
	}
}
