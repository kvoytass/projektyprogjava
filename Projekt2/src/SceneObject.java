import java.awt.Color;
import java.util.ArrayList;

public abstract class SceneObject 
{
	ArrayList<Fragment> body;
	Color color;
	
	public SceneObject()
	{
		body = new ArrayList<Fragment>();
	}
	
	public SceneObject(Color col)
	{
		body = new ArrayList<Fragment>();
		color = col;
	}

	public ArrayList<Fragment> getBody()
	{
		return body;
	}

	public void setBody(ArrayList<Fragment> body)
	{
		this.body = body;
	}
}
