import java.util.ArrayList;

import javax.swing.table.DefaultTableModel;

public class ListTableConverter extends org.jdesktop.beansbinding.Converter<ArrayList<CurrencyData>, DefaultTableModel>
{

	@Override
	public DefaultTableModel convertForward(ArrayList<CurrencyData> l) 
	{
		DefaultTableModel dtm = new DefaultTableModel(0, 0);
		dtm.setColumnIdentifiers(new Object[] { "Code", "Name", "Ask", "Mid", "Bid", "Fetch date" });
		for(CurrencyData cd : l)
		{
			dtm.addRow(cd.rowArgs());
		}
		
		return dtm;
	}

	@Override
	public ArrayList<CurrencyData> convertReverse(DefaultTableModel arg0) 
	{
		// TODO Auto-generated method stub
		return null;
	}

}
