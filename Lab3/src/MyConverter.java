import java.util.ArrayList;

public class MyConverter extends org.jdesktop.beansbinding.Converter<ArrayList<CurrencyData>, String>
{

	@Override
	public String convertForward(ArrayList<CurrencyData> l) 
	{
		// TODO Auto-generated method stub
		if(l.size() == 0) return "No entries";
		
		String s = CurrencyData.getHeaders();
		for(CurrencyData cd : l)
		{
			s += cd.toString() + "\n";
		}
		return s;
	}

	@Override
	public ArrayList<CurrencyData> convertReverse(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}


}