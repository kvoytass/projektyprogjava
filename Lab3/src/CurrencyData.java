import java.util.ArrayList;

public class CurrencyData 
{
	private int id;

	private double bidRate, midRate, askRate;
	private String name, code, effectiveDateMid, effectiveDateBidAsk, NBPnum, fetchDate;
	
	private int fetchStatus = 0;
	
	CurrencyData()
	{
		this.id = (int)(System.currentTimeMillis() / 1000l);
	}
	
	CurrencyData(int _id)
	{
		this.id = _id;
	}
	
	public static CurrencyData getExample()
	{
		CurrencyData cd = new CurrencyData();
		//cd.setId(2);
		cd.setCode("GBP");
		cd.setName("funt brytyjski");
		cd.setNBPnum("1829-42");
		cd.setAskRate(3);
		cd.setMidRate(4);
		cd.setBidRate(5);
		cd.setFetchDate("01.01.1997");
		cd.setEffectiveDateBidAsk("01.01.1997");
		cd.setEffectiveDateMid("01.01.1997");
		cd.setFetchStatus(1);
		
		return cd;
	}
	
	public static String getHeaders()
	{
		//
		return "Code\tName\tBid\tMid\tAsk\tFetch date\n";
	}
	
	@Override
	public String toString()
	{
		
		String s = code + "\t" + name + "\t" + bidRate + "\t" + midRate + "\t" + askRate + "\t" + fetchDate;
		
		if(fetchStatus != 1) s = "Data not fetched";
		
		return s;
	}
	
	public String toDatabaseEntryFormat()
	{
		String tr = "";
		tr += id + ", ";
		tr += "'" + code + "', ";
		tr += "'" + name + "', ";
		tr += "'" + NBPnum + "', ";
		tr += (float)bidRate + ", ";
		tr += (float)midRate + ", ";
		tr += (float)askRate + ", ";
		tr += "'" + fetchDate + "', ";
		tr += "'" + effectiveDateBidAsk + "', ";
		tr += "'" + effectiveDateMid + "', ";
		tr += fetchStatus;
				
		return tr;
	}
	
	public Object[] rowArgs()
	{
		if(fetchStatus != 1) return new Object[] {"Data not fetched", "", "", "", "", ""};
		return new Object[] {code, name, askRate, midRate, bidRate, fetchDate};
	}
	
	public int getId() {
		return id;
	}

	public double getAskRate() {
		return askRate;
	}

	public void setAskRate(double askRate) {
		this.askRate = askRate;
	}

	public double getBidRate() {
		return bidRate;
	}

	public void setBidRate(double bidRate) {
		this.bidRate = bidRate;
	}

	public double getMidRate() {
		return midRate;
	}

	public void setMidRate(double midRate) {
		this.midRate = midRate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getEffectiveDateMid() {
		return effectiveDateMid;
	}

	public void setEffectiveDateMid(String effectiveDateMid) {
		this.effectiveDateMid = effectiveDateMid;
	}

	public String getEffectiveDateBidAsk() {
		return effectiveDateBidAsk;
	}

	public void setEffectiveDateBidAsk(String effectiveDateBidAsk) {
		this.effectiveDateBidAsk = effectiveDateBidAsk;
	}

	public String getNBPnum() {
		return NBPnum;
	}

	public void setNBPnum(String nBPnum) {
		NBPnum = nBPnum;
	}

	public String getFetchDate() {
		return fetchDate;
	}

	public void setFetchDate(String fetchDate) {
		this.fetchDate = fetchDate;
	}

	public int getFetchStatus() {
		return fetchStatus;
	}

	public void setFetchStatus(int fetchStatus) {
		this.fetchStatus = fetchStatus;
	}
	
	public static boolean listContainsEntry(ArrayList<CurrencyData> cdL, CurrencyData entry)
	{
		for(CurrencyData cdOnList : cdL)
		{
			if(cdOnList.getId() == entry.getId()) return true;
		}
		
		return false;
	}
	
	public static ArrayList<CurrencyData> mergeLists(ArrayList<CurrencyData> one, ArrayList<CurrencyData> two)
	{
		ArrayList<CurrencyData> merged = (ArrayList<CurrencyData>) one.clone();
		
		for(CurrencyData cdInTwo : two)
		{
			if(!listContainsEntry(merged, cdInTwo)) merged.add(cdInTwo);
		}
		
		return merged;
	}
}
