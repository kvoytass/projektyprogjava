import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class MyJTable extends JTable
{
	private DefaultTableModel dtm;

	public DefaultTableModel getDtm() {
		return dtm;
	}

	public void setDtm(DefaultTableModel dtm) {
		this.dtm = dtm;
		System.out.println("setDtm :)");
		setModel(dtm);
	}
	
}
