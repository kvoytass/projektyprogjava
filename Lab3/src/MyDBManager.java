import java.sql.*;
import java.util.ArrayList;

public class MyDBManager 
{//
	static final String DATABASE_NAME = "HZNJuWX3gI";
	static final String DATABASE_HOST = "remotemysql.com:3306";
//	static final String DATABASE_NAME = "PWr_ProgJava_CurrParser";
//	static final String DATABASE_HOST = "localhost";
	
	static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";  
	static final String DB_URL = "jdbc:mysql://" + DATABASE_HOST + "/" + DATABASE_NAME + "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
	//cmd: mysqladmin create PWr_ProgJava_CurrParser -u root -p
	
	static final String USER = "HZNJuWX3gI";
	static final String PASS = "OBKeNaC3WK";
//	static final String USER = "wojtek";
//	static final String PASS = "haslo123";
	
	static Connection conn = null;
	
	public static boolean initialize()
	{
		boolean success = false;
		
		try
	    {
		     Class.forName("com.mysql.cj.jdbc.Driver");
		      
		     System.out.println("Connecting to database...");
		     conn = DriverManager.getConnection(DB_URL,USER,PASS);
		      
		     success = true;
	    }
	    catch(Exception e)
	    {
		     e.printStackTrace();
	    }
	    
	    if(!success) conn = null;
	   
	    //if(tableExists("CurrencyDataEntries")) deleteTable("CurrencyDataEntries");
	    if(!tableExists("CurrencyDataEntries")) createTable();
	   
	    System.out.println("Connection established: " + success);
	    return success;
	}
	
	private static void createTable()
	{
		if(isInitialized())
		{
			int res = -1;
			
			String upd = "create table CurrencyDataEntries(" + 
										"id int not null," + 
										"code varchar (3)," + 
										"name varchar (255)," + 
										"nbpnum varchar (255)," + 
										"bidRate float," + 
										"midRate float," + 
										"askRate float," + 
										"fetchDate varchar (255)," + 
										"bidaskDate varchar (255)," + 
										"midDate varchar (255)," + 
										"fetchStatus int not null" + 
										");";
			
			if(tableExists("CurrencyDataEntries"))
			{
				System.out.println("Error: Table already exists!");
			}
			else
			{
				try
				{
					res = executeUpdate(upd);
					System.out.println("CurrencyDataEntries table created :)");
				}
				catch(Exception e)
				{
					System.out.println("Error: createTable failed!");
					e.printStackTrace();
				}
				
				//System.out.println("createTable result: " + res);
				//System.out.println(java.sql.Statement.SUCCESS_NO_INFO);
				//System.out.println(java.sql.Statement.EXECUTE_FAILED);
			}
		}
		
	}
	
	private static void createTestTable(String tableName)
	{
		if(isInitialized())
		{
			int res = -1;
			
			String upd = "create table " + tableName + "(" + 
										"id int not null," + 
										"mytext varchar (255)" + 
										");";
			
			if(tableExists(tableName))
			{
				System.out.println("Error: Table " + tableName + " already exists!");
			}
			else
			{
				
				try
				{
					res = executeUpdate(upd);
				}
				catch(Exception e)
				{
					System.out.println("Error: createTable failed!");
					e.printStackTrace();
				}
				
				System.out.println("creatTestTable result: " + res);
				//System.out.println(java.sql.Statement.SUCCESS_NO_INFO);
				//System.out.println(java.sql.Statement.EXECUTE_FAILED);
			}
		}
		
	}
	
	private static void deleteTable(String tableName)
	{
		if(isInitialized())
		{
			int res = -1;
			
			String upd = "DROP TABLE " + tableName + ";";
			
			try
			{
				res = executeUpdate(upd);
			}
			catch(Exception e)
			{
				System.out.println("Error: deleteTable failed!");
				e.printStackTrace();
			}
			
			System.out.println("deleteTable " + tableName + " result: " + res);
		}
	}
	
	public static void erase()
	{
		if(isInitialized())
		{
			if(tableExists("CurrencyDataEntries")) deleteTable("CurrencyDataEntries");
			createTable();
		}
	}
	
	private static boolean tableExists(String tableName)
	{
		ResultSet rs = executeQuery("SELECT count(*) " + 
									"FROM information_schema.TABLES "+ 
									"WHERE (TABLE_SCHEMA = '" + DATABASE_NAME + "' AND " + 
									"TABLE_NAME = '" + tableName + "');");
		
		int ct = -1;
		
		try
		{
		    rs.next();
	  		ct = rs.getInt("count(*)");
		}
		catch(Exception e)
		{
			System.out.println("Error: table existence check failed");
			e.printStackTrace();
		}
		
		//System.out.println("Table " + tableName + " exists: " + (ct == 1));
		return (ct == 1);
	}
	
	private static int executeUpdate(String upd)
	{
		Statement xstmt;
		int xrs = -1;
		try
		{
			xstmt = conn.createStatement();
			xrs = xstmt.executeUpdate(upd);
			
			xstmt.close();
		}
		catch(Exception e)
		{
			System.out.println("Error: executeUpdate failed");
			e.printStackTrace();
		}
		
		return xrs;
	}
	
	private static ResultSet executeQuery(String qry)
	{
		Statement stmt;
		ResultSet rs;
		try
		{
		      stmt = conn.createStatement();
		      rs = stmt.executeQuery(qry);
		}
		catch (Exception e)
		{
			System.out.println("Error: executeQuery failed");
			e.printStackTrace();
			return null;
		}
		
		return rs;
	}
	
	public static ArrayList<String> getTablesList()
	{
		if(isInitialized())
		{
			ArrayList<String> tablesList = new ArrayList<String>();
			ResultSet rs = executeQuery("show tables;");
			
			try
			{
				while(rs.next())
				{
					String tname = rs.getString("Tables_in_" + DATABASE_NAME.toLowerCase());
					tablesList.add(tname);
				}
				
				return tablesList;
			}
			catch(Exception e)
			{
				System.out.println("Error: getTablesList failed");
				e.printStackTrace();
			}
		}
		
		return null;
	}
	
	public static ArrayList<CurrencyData> readEntriesFromDatabase(boolean fetchedOnly)
	{
		if(isInitialized())
		{
			ArrayList<CurrencyData> entriesList = new ArrayList<CurrencyData>();
			ResultSet rs = executeQuery("SELECT * FROM CurrencyDataEntries;");
			
			try
			{
				while(rs.next())
				{
					CurrencyData cd = new CurrencyData(rs.getInt("id"));
					//cd.setId(rs.getInt("id"));
					cd.setCode(rs.getString("code"));
					cd.setName(rs.getString("name"));
					cd.setNBPnum(rs.getString("nbpnum"));
					cd.setBidRate(rs.getDouble("bidRate"));
					cd.setMidRate(rs.getDouble("midRate"));
					cd.setAskRate(rs.getDouble("askRate"));
					cd.setFetchDate(rs.getString("fetchDate"));
					cd.setEffectiveDateBidAsk(rs.getString("bidaskDate"));
					cd.setEffectiveDateMid(rs.getString("midDate"));
					cd.setFetchStatus(rs.getInt("fetchStatus"));
					
					if(fetchedOnly && cd.getFetchStatus() != 1) continue;
					entriesList.add(cd);
				}
				
				return entriesList;
			}
			catch(Exception e)
			{
				System.out.println("Error: readEntriesFromDatabase failed");
				e.printStackTrace();
			}
		}
		
		return null;
	}
	
	private static void addEntryToDatabase(CurrencyData toAdd)
	{
		if(isInitialized())
		{
			int res = -1;
			
			String upd = "INSERT INTO CurrencyDataEntries VALUES (" + toAdd.toDatabaseEntryFormat() + ");";
			
			try
			{
				res = executeUpdate(upd);
			}
			catch(Exception e)
			{
				System.out.println("Error: addEntryToDatabase failed!");
				e.printStackTrace();
			}
			
			//System.out.println("addEntryToDatabase result: " + res);
		}
	}
	
	public static void addEntriesToDatabase(ArrayList<CurrencyData> cdL, boolean fetchedOnly)
	{
		if(isInitialized() && cdL != null)
		{
			ArrayList<CurrencyData> currentlyInDB = readEntriesFromDatabase(false);
			int count = 0;
			for(CurrencyData toAdd : cdL)
			{
				if((!fetchedOnly) || (fetchedOnly && toAdd.getFetchStatus() == 1))
				{
					if(!CurrencyData.listContainsEntry(currentlyInDB, toAdd)) 
					{
						addEntryToDatabase(toAdd);
						count++;
					}
				}
			}
			
			System.out.println("Added " + count + " entries to Database");
		}
	}
	
	public static boolean isInitialized() { return (conn != null); }
	
	public static void main(String[] args) 
	{
		if(!isInitialized()) initialize();
		
		getTablesList();
		ArrayList<CurrencyData> test =  readEntriesFromDatabase(false);
		System.out.println("testlen: " + test.size());

		CurrencyData cd = CurrencyData.getExample();
		
		addEntryToDatabase(cd);
		
		test =  readEntriesFromDatabase(false);
		System.out.println("testlen: " + test.size());
		for(CurrencyData c : test) {
			System.out.println(c.toString());
		}
	}
}
