import java.awt.EventQueue;

import javax.swing.*;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.beansbinding.AutoBinding;
import org.jdesktop.beansbinding.Bindings;
import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MyFetcher extends JFrame
{
	private final static boolean autoLoadFromDBOnStart = true;
	
	private static ArrayList<CurrencyData> entries = new ArrayList<CurrencyData>();
	public ArrayList<CurrencyData> getEntries() { return entries; }
	public void setEntries(ArrayList<CurrencyData> entries) { this.entries = entries; }
	public ArrayList<CurrencyData> addEntryToClone(CurrencyData toAdd) 
	{ 
		ArrayList<CurrencyData> n = (ArrayList<CurrencyData>) entries.clone();
		n.add(toAdd); 
		
		return n; 
	}
	private static BeanProperty<MyFetcher, ArrayList<CurrencyData>> cdlistBP;
	
	
	public class MyHandler extends DefaultHandler 
	{
		MyHandler(CurrencyData cd, boolean _tableC)
		{
			curData = cd;
			elements = new HashMap<String, Boolean>();
			tableC = _tableC;
			curData.setFetchDate(GetCurrentDate());
		}
		CurrencyData curData;
		Map<String, Boolean> elements;
		private StringBuilder data;
		boolean tableC;

		@Override
		public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException 
		{
			elements.put(qName, true);
			// create the data container
			data = new StringBuilder();
		}

		@Override
		public void endElement(String uri, String localName, String qName) throws SAXException 
		{
			if(elements.containsKey(qName))
			{
				//System.out.println("elements.containsKey(qName)");
				if(qName.equals("Currency"))
				{
					curData.setName(data.toString());
				}
				else if(qName.equals("Code"))
				{
					curData.setCode(data.toString());
				}
				else if(qName.equals("No"))
				{
					curData.setNBPnum(data.toString());
				}
				else if(qName.equals("EffectiveDate"))
				{
					if(tableC) curData.setEffectiveDateBidAsk(data.toString());
					else curData.setEffectiveDateMid(data.toString());
				}
				else if(qName.equals("Ask"))
				{
					curData.setAskRate(Double.parseDouble(data.toString()));
				}
				else if(qName.equals("Bid"))
				{
					curData.setBidRate(Double.parseDouble(data.toString()));
				}
				else if(qName.equals("Mid"))
				{
					curData.setMidRate(Double.parseDouble(data.toString()));
				}
			}
			
			elements.put(qName, false);
		}

		@Override
		public void characters(char ch[], int start, int length) throws SAXException 
		{
			data.append(new String(ch, start, length));
		}
	}
	

	private JPanel contentPane;
	private JTextField curBox;
	private JTextArea textArea;

	public static void main(String[] args) 
	{
		EventQueue.invokeLater(new Runnable() 
		{
			public void run() 
			{
				try 
				{
					MyFetcher frame = new MyFetcher();
					frame.setVisible(true);
				} catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
		});
		
	    Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() 
	    {
	        public void run() 
	        {
	            System.out.println("In shutdown hook - begin");
	    		if(MyDBManager.isInitialized() && entries.size() > 0) MyDBManager.addEntriesToDatabase(entries, true);
	            System.out.println("In shutdown hook - end");
	        }
	    }, "Shutdown-thread"));
	}

	/**
	 * Create the frame.
	 */
	public MyFetcher() 
	{
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1000, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{0, 0, 0, 0, 0, };
		gbl_contentPane.rowHeights = new int[]{0, 0, 0, 0, 0, 0};
		gbl_contentPane.columnWeights = new double[]{1.0, 1.0, 3.0, 1.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{1.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);

		JTextArea dataArea;
		dataArea = new JTextArea();
		dataArea.setTabSize(12);
		dataArea.setEditable(false);
		GridBagConstraints gbc_dataArea = new GridBagConstraints();
		gbc_dataArea.gridwidth = 4;
		gbc_dataArea.insets = new Insets(0, 0, 5, 0);
		gbc_dataArea.fill = GridBagConstraints.BOTH;
		gbc_dataArea.gridx = 0;
		gbc_dataArea.gridy = 0;
		contentPane.add(dataArea, gbc_dataArea);
		
		curBox = new JTextField();
		curBox.addKeyListener(new KeyAdapter()
		{
			@Override
			public void keyPressed(KeyEvent arg0) 
			{
				if(arg0.getKeyCode() == KeyEvent.VK_ENTER) OnFetchButtonClicked();
			}
		});
		GridBagConstraints gbc_curBox = new GridBagConstraints();
		gbc_curBox.gridwidth = 2;
		gbc_curBox.insets = new Insets(0, 0, 5, 5);
		gbc_curBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_curBox.gridx = 0;
		gbc_curBox.gridy = 1;
		contentPane.add(curBox, gbc_curBox);
		curBox.setColumns(10);
		initDataBindings(dataArea);
		
		JButton fetchButton = new JButton("Fetch data");
		fetchButton.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseClicked(MouseEvent arg0) 
			{
				OnFetchButtonClicked();
			}
		});
		
		JLabel lblDatabaseControls = new JLabel("Database controls:");
		GridBagConstraints gbc_lblDatabaseControls = new GridBagConstraints();
		gbc_lblDatabaseControls.insets = new Insets(0, 0, 5, 0);
		gbc_lblDatabaseControls.gridx = 3;
		gbc_lblDatabaseControls.gridy = 1;
		contentPane.add(lblDatabaseControls, gbc_lblDatabaseControls);
		GridBagConstraints gbc_fetchButton = new GridBagConstraints();
		gbc_fetchButton.insets = new Insets(0, 0, 5, 5);
		gbc_fetchButton.gridx = 0;
		gbc_fetchButton.gridy = 2;
		contentPane.add(fetchButton, gbc_fetchButton);
		
		JButton btnEraseEntries = new JButton("Erase entries");
		btnEraseEntries.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) { OnClearEntriesButtonClicked(); }
		});
		btnEraseEntries.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		GridBagConstraints gbc_btnEraseEntries = new GridBagConstraints();
		gbc_btnEraseEntries.insets = new Insets(0, 0, 5, 5);
		gbc_btnEraseEntries.gridx = 1;
		gbc_btnEraseEntries.gridy = 2;
		contentPane.add(btnEraseEntries, gbc_btnEraseEntries);
		
		JButton btnLoadFromDb = new JButton("Load from DB");
		btnLoadFromDb.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) { OnLoadFromDBButtonClicked(); }
		});
		GridBagConstraints gbc_btnLoadFromDb = new GridBagConstraints();
		gbc_btnLoadFromDb.insets = new Insets(0, 0, 5, 0);
		gbc_btnLoadFromDb.gridx = 3;
		gbc_btnLoadFromDb.gridy = 2;
		contentPane.add(btnLoadFromDb, gbc_btnLoadFromDb);
		
		JButton btnSaveToDb = new JButton("Save to DB");
		btnSaveToDb.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) { OnSaveToDBButtonClicked(); }
		});
		GridBagConstraints gbc_btnSaveToDb = new GridBagConstraints();
		gbc_btnSaveToDb.insets = new Insets(0, 0, 5, 0);
		gbc_btnSaveToDb.gridx = 3;
		gbc_btnSaveToDb.gridy = 3;
		contentPane.add(btnSaveToDb, gbc_btnSaveToDb);
		
		JButton btnEraseDb = new JButton("Erase DB");
		btnEraseDb.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) { OnEraseDBButtonClicked(); }
		});
		GridBagConstraints gbc_btnEraseDb = new GridBagConstraints();
		gbc_btnEraseDb.gridx = 3;
		gbc_btnEraseDb.gridy = 4;
		contentPane.add(btnEraseDb, gbc_btnEraseDb);
		

		MyDBManager.initialize();
		if(autoLoadFromDBOnStart) LoadFromDB();
		
	}
	
	private void OnLoadFromDBButtonClicked()
	{
		System.out.println("OnLoadFromDBButtonClicked()");
		LoadFromDB();
	}
	
	private void LoadFromDB()
	{
		ArrayList<CurrencyData> mergedEntriesAndDB = CurrencyData.mergeLists(entries, MyDBManager.readEntriesFromDatabase(false));
		cdlistBP.setValue(this, mergedEntriesAndDB);
	}
	
	private void OnSaveToDBButtonClicked()
	{
		System.out.println("OnSaveToDBButtonClicked()");
		MyDBManager.addEntriesToDatabase(entries, true);
	}
	
	private void OnEraseDBButtonClicked()
	{
		System.out.println("OnEraseDBButtonClicked()");
		int dialogButton = JOptionPane.YES_NO_OPTION;
		int dialogResult = JOptionPane.showConfirmDialog(this, "Are you sure you want to erase database?", "Warning", dialogButton);
		if(dialogResult == 0) 
		{
			MyDBManager.erase();
		}  
		else System.out.println("DB erasing canceled");
	}
	
	private void OnClearEntriesButtonClicked()
	{
		System.out.println("OnClearEntriesButtonClicked()");
		int dialogButton = JOptionPane.YES_NO_OPTION;
		int dialogResult = JOptionPane.showConfirmDialog(this, "Are you sure you want to clear entries?", "Warning", dialogButton);
		if(dialogResult == 0) 
		{
			cdlistBP.setValue(this, new ArrayList<CurrencyData>());
		} 
		else System.out.println("Entries clear canceled");
	}
	
	private void OnFetchButtonClicked()
	{
		System.out.println("OnFetchButtonClicked()");
		
		CurrencyData curD = FetchCurrencyData(curBox.getText());
		
		System.out.println(curD.toString());
	}
	
	private CurrencyData FetchCurrencyData(String curCode)
	{
		if(curCode.length() != 3)
		{
			CurrencyData r = new CurrencyData();
			r.setFetchStatus(-1);
			return r;
		}
		
		String urlA = "http://api.nbp.pl/api/exchangerates/rates/a/" + curCode.toLowerCase() + "/?format=xml";
		String urlC = "http://api.nbp.pl/api/exchangerates/rates/c/" + curCode.toLowerCase() + "/?format=xml";
		CurrencyData curD = new CurrencyData();

		try 
		{
			SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
			parser.parse(urlA, new MyHandler(curD, false));
			parser.parse(urlC, new MyHandler(curD, true));
			curD.setFetchStatus(1);
		} catch (Exception e) 
		{
			if(e instanceof java.io.FileNotFoundException && e.getMessage().contains("exchangerates/rates/c/"))
			{
				curD.setAskRate(0);
				curD.setBidRate(0);
				curD.setFetchStatus(1);
			}
			else
			{
				System.out.println(e.toString());
				curD.setFetchStatus(-1);
			}
		}
		
		cdlistBP.setValue(this, addEntryToClone(curD));
		//ListTableConverter ltc = new ListTableConverter();
		//table.setModel(ltc.convertForward(entries));
		
		if(curD.getFetchStatus() == 1) curBox.setText("");
		return curD;
	}
	
	public static String GetCurrentDate()
	{	
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		return dateFormat.format(date); //2016/11/16 12:08:43
	}
	
	protected void initDataBindings(JTextArea dataArea) 
	{
		BeanProperty<JTextArea, String> jTextAreaBeanProperty = BeanProperty.create("text");
		
		cdlistBP = BeanProperty.create("entries");
		AutoBinding<MyFetcher, ArrayList<CurrencyData>, JTextArea, String> autoBinding = Bindings.createAutoBinding(UpdateStrategy.READ, this, cdlistBP, dataArea, jTextAreaBeanProperty);
		autoBinding.setConverter(new MyConverter());
		autoBinding.bind();
	}
}
