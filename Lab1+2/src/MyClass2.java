import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.util.Random;
import java.util.Timer;

import javax.swing.JButton;
import javax.swing.JFrame;
import java.util.TimerTask;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public class MyClass2 extends JFrame 
{
	private static final int FPS = 40;
	private static final double speed = 600; //pixels per second
	private static final int clickToQuit = 5;
	
    public MyClass2() 
    {
        initWindow();
    }

    private void initWindow() 
    {
    	//set layout:
        Container pane = getContentPane();
        pane.setLayout(new FlowLayout());
    	
        //create and add button:
        JButton runningButton = new JButton("Click here");
        runningButton.addActionListener((event) -> OnButtonClicked(runningButton));
        pane.add(runningButton);
        
        //set up window parameters
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(screenSize);
        setLocation(0,0);
        setUndecorated(true);
        setBackground(new Color(1.0f,1.0f,1.0f,0.0f));
        setAlwaysOnTop(true);
        
        //resize button to 10% of screen
        Dimension bSize = new Dimension();
        bSize.setSize((int)(0.1*screenSize.getWidth()), (int)(0.1*screenSize.getHeight()));
        runningButton.setPreferredSize(bSize);
    }
    
    static int clickCounter = 0;
    
    void OnButtonClicked(JButton btn)
    {
    	clickCounter++;
    	if(clickCounter == clickToQuit) System.exit(0); //exit after 8 clicks
    	
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        
        Random rand = new Random();
        
        double newPosition[] = { rand.nextDouble()*0.9*screenSize.getWidth(), rand.nextDouble()*0.9*screenSize.getHeight() };
        double curPosition[] = { btn.getLocation().getX(), btn.getLocation().getY() };
        
        /* System.out.println("current Position: " + curPosition[0] + ", " + curPosition[1]);
        System.out.println("new Position: " + newPosition[0] + ", " + newPosition[1]); */
        
        List<double[]> moveSequence = GenerateMoveSequence(curPosition, newPosition);
        
        double distance = CalculateDistance(curPosition, newPosition);
        double delayBetweenFrames = (distance / moveSequence.size()) / speed;
        
        /*
        System.out.println("moveSequence length = " + moveSequence.size());
        System.out.println("totalDistance = " + distance);
        System.out.println("moveSequence first: " + moveSequence.get(0)[0] + ", " + moveSequence.get(0)[1]);
        System.out.println("moveSequence last: " + moveSequence.get(moveSequence.size()-1)[0] + ", " + moveSequence.get(moveSequence.size()-1)[1]);
        */
        
        for(int i = 0; i < moveSequence.size(); i++)
        {
	        Timer stepTimer = new Timer();
	        stepTimer.schedule(new TimerTask() 
	        {
	
				@Override
				public void run() 
				{
					double[] dim = moveSequence.remove(0);
					btn.setLocation((int)dim[0], (int)dim[1]);
					
				} 
			}, (long) (1000 * i * delayBetweenFrames));
        }
    }
    
    
    ArrayList<double[]> GenerateMoveSequence(double[] initialDim, double[] finalDim)
    {
    	double distance = CalculateDistance(initialDim, finalDim);
    	double widthDif = finalDim[0] - initialDim[0];
    	double heightDif = finalDim[1] - initialDim[1];
    	
    	int numOfFrames = CalculateNumberOfFrames(distance);
    	
    	ArrayList<double[]> sequence = new ArrayList<double[]>();
    	for(int i = 1; i <= numOfFrames; i++)
    	{
    		double[] toAdd = {initialDim[0] + i * widthDif / numOfFrames, initialDim[1] + i * heightDif / numOfFrames};
    		sequence.add(toAdd);
    	}
    	
    	return sequence;
    }
    
    static double CalculateDistance(double[] initialDim, double[] finalDim)
    {
    	double widthDif = finalDim[0] - initialDim[0];
    	double heightDif = finalDim[1] - initialDim[1];
    	double distance = Math.sqrt(Math.pow(widthDif, 2)+Math.pow(heightDif, 2));
    	
    	return distance;
    }
    
    static int CalculateNumberOfFrames(double distance)
    {
    	double speedPerFrame = speed / FPS;
    	int numOfFrames = (int)( Math.ceil(distance / speedPerFrame));
    	
    	return numOfFrames;
    }

	public static void main(String[] args) 
	{
        MyClass2 mc = new MyClass2();
        mc.setVisible(true);
	}

}
