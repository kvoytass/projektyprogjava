import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.util.Random;
import javax.swing.JButton;
import javax.swing.JFrame;

@SuppressWarnings("serial")
public class MyClass extends JFrame
{
    public MyClass() 
    {
        initWindow();
    }

    private void initWindow() 
    {
    	//set layout:
        Container pane = getContentPane();
        pane.setLayout(new FlowLayout());
    	
        //create and add button:
        JButton runningButton = new JButton("Click hereV1");
        runningButton.addActionListener((event) -> OnButtonClicked(runningButton));
        pane.add(runningButton);
        
        //set up window parameters
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        //System.out.println("W: " + screenSize.getWidth() + " H: " + screenSize.getHeight());
        setSize(screenSize);
        setLocation(0,0);
        setUndecorated(true);
        setBackground(new Color(1.0f,1.0f,1.0f,0.0f));
        setAlwaysOnTop(true);
        
        //resize button to 10% of screen
        Dimension bSize = new Dimension();
        bSize.setSize((int)(0.1*screenSize.getWidth()), (int)(0.1*screenSize.getHeight()));
        runningButton.setPreferredSize(bSize);
    }
    
    static int clickCounter = 0;
    
    void OnButtonClicked(JButton btn)
    {
    	clickCounter++;
    	if(clickCounter == 5) System.exit(0); //exit after 5 clicks
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Random rand = new Random();
        btn.setLocation((int)(rand.nextDouble()*0.9*screenSize.getWidth()), (int)(rand.nextDouble()*0.9*screenSize.getHeight()));
    }

    /*
	public static void main(String[] args) 
	{
        MyClass mc = new MyClass();
        mc.setVisible(true);
	} */

}
