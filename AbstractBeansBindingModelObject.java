public abstract class AbstractBeansBindingModelObject 
{
	private final java.beans.PropertyChangeSupport propertyChangeSupport = new java.beans.PropertyChangeSupport(this);

	public void addPropertyChangeListener(java.beans.PropertyChangeListener listener) 
	{
		propertyChangeSupport.addPropertyChangeListener(listener);
	}

	public void addPropertyChangeListener(String propertyName, java.beans.PropertyChangeListener listener) 
	{
		propertyChangeSupport.addPropertyChangeListener(propertyName, listener);
	}

	public void removePropertyChangeListener(java.beans.PropertyChangeListener listener) 
	{
		propertyChangeSupport.removePropertyChangeListener(listener);
	}

	public void removePropertyChangeListener(String propertyName, java.beans.PropertyChangeListener listener) 
	{
		propertyChangeSupport.removePropertyChangeListener(propertyName, listener);
	}

	protected void firePropertyChange(String propertyName, Object oldValue, Object newValue) 
	{
		propertyChangeSupport.firePropertyChange(propertyName, oldValue, newValue);
	}
}